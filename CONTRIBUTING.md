Hi, and thank you for your interest !

# Contribute

Contributions are very welcome! Note however that as the sole maintainer with a non-programming related job, it might take some time until I can look at a given merge request or issue.

Making a contribution follows the common gitlab workflow:

- [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) this repository, and clone your fork locally.
- [Optional] To ensure that CI does not reject your changes, use the available pre-push [hook](https://git-scm.com/docs/githooks):
  ```sh
  git config --local core.hooksPath .githooks
  ```
- Create a new git branch (using e.g. `git switch -c new-branch`).
- Hack, code, modify... Try to follow the [Code style](#Code-style)
- Push your branch to your fork.
- Create a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).

# Description of the crate

If you're already well versed in the LR generation algorithm, but are troubled by the specifics of this crate, you may be interested in:

- The description of the IELR algorithm: <https://www.sciencedirect.com/science/article/pii/S0167642309001191>
- How precedence annotations work, and how they change the generated tables: [operator precedence](<./theory/operator precedence.adoc>).

# Code style

## Formatting

Keep the files formatted using `cargo fmt`.

## Tests

- You should run the tests with `cargo test` before submitting your merge request to avoid surprises. If tests do not pass, the merge request will get rejected.
- Internals should be tested on a best-effort basis : try to take inspiration from the existing tests.
- `cargo deny` is run on merge request, so be careful when adding dependencies.
  In general, adding dependencies is discouraged without a very strong motivation.

## Warnings and clippy

On merge requests, the command `cargo clippy -- -D warnings` is run. This means that a merge request containing warnings (including clippy ones) will not pass.

## Minimal Rust version

The [rust-version](https://doc.rust-lang.org/cargo/reference/manifest.html#the-rust-version-field) is specified in 'Cargo.toml'.
