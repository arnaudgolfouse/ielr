/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

//! Type aliases and indices used all over the crate.

// REMARK: changing some of those is **not** a backward-compatible change. If you
// want to change something (for performance, or to avoid overflows) in a minor
// release, use a semver checker (for example, cargo-semver-checks)

pub(crate) type NonZero<T> = <T as NonZeroT>::Type;

pub trait NonZeroT {
    type Type;
}

impl NonZeroT for u64 {
    type Type = std::num::NonZeroU64;
}
impl NonZeroT for u32 {
    type Type = std::num::NonZeroU32;
}
impl NonZeroT for u16 {
    type Type = std::num::NonZeroU16;
}
impl NonZeroT for u8 {
    type Type = std::num::NonZeroU8;
}

/// Used in the definition of [`crate::input::Token`] and [`crate::output::Lookahead`].
pub(crate) type PlainToken = u16;
/// Used in the definition of [`crate::input::Node`].
pub(crate) type PlainNode = u16;

/// Index of a symbol in the right-hand side of a production.
// NOTE: if this is changed, some documentation in `input/grammar.rs` should also change
pub(crate) type SymbolIdx = u16;

/// Index of the right-hand side of a production in
/// [`grammar.productions[lhs]`](crate::input::Grammar::productions).
// NOTE: if this is changed, some documentation in `input/grammar.rs` should also change
pub(crate) type ProdIdxRaw = u16;

pub(crate) type StateIdxRaw = u32;

pub(crate) struct StateIdx<Kind>(StateIdxRaw, std::marker::PhantomData<*const Kind>);

/// Index of an item in a state.
pub(crate) type ItemIdx = u32;

/// Global index of a 'goto' action in a state.
///
/// # Note
/// When precedence annotations are used, this might designate a _single_ item with
/// index `0` in a state.
pub(crate) type GotoIdx = u32;

//==================================================================
//==================== StateIdx IMPLEMENTATIONS ====================
//==================================================================

impl<Kind> Clone for StateIdx<Kind> {
    fn clone(&self) -> Self {
        *self
    }
}

impl<Kind> Copy for StateIdx<Kind> {}

impl<Kind> PartialEq for StateIdx<Kind> {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl<Kind> Eq for StateIdx<Kind> {}

impl<Kind> std::hash::Hash for StateIdx<Kind> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.hash(state);
    }
}

impl<Kind> std::fmt::Debug for StateIdx<Kind> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_tuple("StateIdx").field(&self.0).finish()
    }
}

impl<Kind> From<StateIdx<Kind>> for crate::output::StateIdx {
    fn from(s: StateIdx<Kind>) -> Self {
        Self(s.0)
    }
}

impl<Kind> StateIdx<Kind> {
    pub(crate) const fn new(index: StateIdxRaw) -> Self {
        Self(index, std::marker::PhantomData)
    }

    pub(crate) const fn get(self) -> StateIdxRaw {
        self.0
    }
}
