/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

//! Various data structures used in this crate.

mod bitset;
mod visit;

pub(crate) use self::{
    bitset::BitSet,
    visit::{ReVisit, Visit},
};

/// The set type used by this crate.
///
/// Mapped to `fnv::FnvHashSet`.
pub(crate) type Set<T> = fnv::FnvHashSet<T>;
/// The map type used by this crate.
///
/// Mapped to `fnv::FnvHashMap`.
pub(crate) type Map<K, V> = fnv::FnvHashMap<K, V>;

/// Default hasher.
pub(crate) type Hasher = std::collections::hash_map::RandomState;
/// Fast hasher.
pub(crate) type FastHasher = fnv::FnvBuildHasher;
