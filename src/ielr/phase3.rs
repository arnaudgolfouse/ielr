/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

//! Phase 3: Split states
//!
//! In this phase, we recompute the LR(1) tables, using annotation from phase 2 to
//! split LALR states.

// TODO: tests the functions in this module.

use super::{phase2::Annotation, Phase2};
use crate::{
    grammar_data::{
        ConflictResolutionCache, ContributionIndex, ContributionSelection, GrammarData, ReduceIdx,
        ShiftOrReduce,
    },
    indices::{ItemIdx, StateIdxRaw},
    input::Symbol,
    lr::{LRState, LRTable, StateList, LALR, LR1},
    output::{error::Conflict, Error, Lookahead},
    structures::{BitSet, Map, Set},
    GotoIdx, StateIdx,
};
use std::ops::{Deref, DerefMut};

pub(crate) struct Phase3<'a> {
    phase2: Phase2<'a>,
    pub(crate) lr1_table: LRTable<LR1>,
    dominant_contribution_alloc: DominantContributionAlloc,
    lookaheads_recomputed: BitSet<StateIdxRaw>,
    /// Maps LR1 to LALR states.
    lalr_isocores: Vec<StateIdx<LALR>>,
    isocores_next: Vec<StateIdx<LR1>>,
}

/// Keep some data to avoid allocations in [`Phase3::dominant_contribution`].
struct DominantContributionAlloc {
    dominant_contribution_conflicting: Vec<ContributionIndex>,
    items_lookaheads_contain_t: BitSet<ItemIdx>,
    /// Efficient [`crate::output::Statistics::conflict_resolution_used`].
    solutions_used: BitSet,
}

impl DominantContributionAlloc {
    fn new(grammar_data: &GrammarData) -> Self {
        Self {
            dominant_contribution_conflicting: Vec::new(),
            items_lookaheads_contain_t: BitSet::new(),
            solutions_used: BitSet::with_capacity(grammar_data.conflict_solutions.len()),
        }
    }
}

impl<'a> Phase3<'a> {
    pub(crate) fn new(phase2: Phase2<'a>) -> Self {
        let lr1_table = LRTable::new_lr1(phase2.lalr_table);
        let dominant_contribution_alloc = DominantContributionAlloc::new(phase2.grammar_data);
        Self {
            phase2,
            lr1_table,
            dominant_contribution_alloc,
            lookaheads_recomputed: BitSet::new(),
            lalr_isocores: Vec::new(),
            isocores_next: Vec::new(),
        }
    }

    /// Fill reductions in the table, and returns it, together with the accumulated
    /// statistics.
    ///
    /// # Warning
    /// Conflicts must have been resolved prior to this: else, this risk erasing some
    /// 'shift' actions.
    pub(crate) fn get_table(&mut self) -> crate::output::Table {
        let mut table = LRTable {
            states: StateList::new(),
            starting_states: Map::default(),
        };
        std::mem::swap(&mut self.lr1_table, &mut table);
        let mut table: crate::output::Table = table.into();
        table.finish(self.grammar_data);

        table
    }

    /// Check that `s` is compatible with `lookaheads_list`.
    ///
    /// That is, check that for each annotation in `s`, the dominant contribution of
    /// `s` and `lookaheads_list` (as selected by this annotation) is the same.
    fn are_compatible(
        &mut self,
        s: StateIdx<LR1>,
        lookaheads_list: &[Set<Lookahead>],
    ) -> Result<bool, Error> {
        // Check for errors even if this is the first time we compute this state.
        let lookaheads_recomputed = self.lookaheads_recomputed.contains(s.get());
        let state = &self.lr1_table.states[s];
        let lalr_s = self.lalr_isocores[s.get() as usize];
        for annotation in &self.phase2.annotation_lists[lalr_s.get() as usize] {
            let contrib1 = match Self::dominant_contribution(
                self.phase2.grammar_data,
                &mut self.dominant_contribution_alloc,
                &mut self.phase2.phase1.conflict_resolution_cache,
                annotation,
                lookaheads_list.iter(),
            ) {
                Ok(Some(c)) => c,
                Ok(None) => continue,
                Err(conflict) => {
                    return Err(Error::Conflict {
                        state: annotation.conflict.s.into(),
                        lalr_tables: self.get_table(),
                        conflict,
                    })
                }
            };
            let contrib2 = if lookaheads_recomputed {
                match Self::dominant_contribution(
                    self.phase2.grammar_data,
                    &mut self.dominant_contribution_alloc,
                    &mut self.phase2.phase1.conflict_resolution_cache,
                    annotation,
                    state.core_items().iter().map(|item| &item.lookaheads),
                ) {
                    Ok(Some(c)) => Some(c),
                    Ok(None) => continue,
                    // This is unreachable, because `lookaheads_recomputed` means we already merged some new lookaheads into this state, meaning there was only one dominant contribution.
                    Err(_) => unreachable!(),
                }
            } else {
                None
            };
            if let Some(contrib2) = contrib2 {
                if contrib1 != contrib2 {
                    return Ok(false);
                }
            }
        }
        Ok(true)
    }

    /// Regenerate the LR table, using the annotations computed in the previous
    /// phases.
    pub(crate) fn regenerate(&mut self) -> Result<(), Error> {
        self.split_states()?;

        self.statistics.lr1_states_count =
            self.lr1_table.states.len().get() as usize - self.statistics.lalr_states_count;
        let solutions_len = self.grammar_data.conflict_solutions.len();
        self.statistics
            .conflict_resolution_used
            .reserve(solutions_len);
        for index in self.dominant_contribution_alloc.solutions_used.iter() {
            self.phase2.statistics.conflict_resolution_used.push(index);
        }
        Ok(())
    }

    /// For a LR(1) state `s` and each core item `i` of `s`, get the lookaheads that
    /// may appear in `i` (according to the LALR table), and contribute to some
    /// conflict.
    fn lookahead_set_filters(&self, s: StateIdx<LR1>) -> Vec<Set<Lookahead>> {
        let lalr_s = self.lalr_isocores[s.get() as usize];
        let annotations = &self.annotation_lists[lalr_s.get() as usize];
        let mut filters =
            vec![Set::default(); self.lalr_table.states[lalr_s].core_items_len as usize];
        for annotation in annotations {
            for reduce in &annotation.possible_contributions.reduces {
                let reduce = if let Some(r) = reduce { r } else { continue };
                for j in reduce.iter() {
                    filters[j as usize].insert(annotation.conflict.lookahead);
                }
            }
        }
        filters
    }

    /// Compute the follow set for the non-core item `g` in state `state`.
    fn compute_goto_follow_set(&self, state: &LRState<LR1>, g: GotoIdx) -> Set<Lookahead> {
        let mut result = Set::default();
        for &l in &self.always_follows[g] {
            result.insert(l);
        }
        for core_index in self.follow_kernel_items[g].iter() {
            for &l in &state.items[core_index as usize].lookaheads {
                result.insert(l);
            }
        }

        result
    }

    /// Propagate the lookaheads of `s` into `s_prime`.
    ///
    /// # Precondition
    /// `s_prime` is a successor of `s` (in the LR(1) state machine).
    ///
    /// # Returns
    /// A list of lookahead sets of length `s_prime.core_items_len`.
    ///
    /// # Note
    /// Since `s_prime` is a successor, it isn't a start state. Thus all of its core
    /// items have an `index > 0`.
    fn propagate_lookaheads(
        &self,
        s: StateIdx<LR1>,
        s_prime: StateIdx<LR1>,
    ) -> Vec<Set<Lookahead>> {
        let mut result = Vec::new();
        let lalr_s = self.lalr_isocores[s.get() as usize];
        let lalr_s_prime = self.lalr_isocores[s_prime.get() as usize];
        let state = &self.lr1_table.states[s];
        let state_prime = &self.lr1_table.states[s_prime];
        let predecessors_items = &self.predecessors[lalr_s_prime.get() as usize][&lalr_s];
        for ((item, &pred_idx), mut set) in state_prime
            .items
            .iter()
            .zip(predecessors_items)
            .zip(self.lookahead_set_filters(s_prime))
        {
            if item.index > 1 {
                let pred_item = &state.items[pred_idx as usize];
                set = set.intersection(&pred_item.lookaheads).copied().collect();
            } else {
                let g: GotoIdx = super::Phase0::get_goto(
                    self.lalr_table,
                    &self.gotos_in_state,
                    lalr_s,
                    pred_idx,
                );
                let computed = self.compute_goto_follow_set(state, g);
                set = set.intersection(&computed).copied().collect();
            }
            result.push(set);
        }
        result
    }

    // TODO(perf): it is probably possible to cache some computations.
    fn split_states(&mut self) -> Result<(), Error> {
        for s in self.lr1_table.states.all_indices() {
            self.lalr_isocores.push(StateIdx::new(s.get()));
            self.isocores_next.push(s);
        }
        // We need to check starting states here, because they are not checked later !
        for s in self
            .lr1_table
            .starting_states
            .values()
            .copied()
            .collect::<Vec<_>>()
        {
            let lalr_s = self.lalr_isocores[s.get() as usize];
            let lookaheads_list: Vec<_> = self.lalr_table.states[lalr_s]
                .core_items()
                .iter()
                .map(|item| item.lookaheads.clone())
                .collect();
            self.are_compatible(s, &lookaheads_list)?;
        }
        for s in self.lr1_table.states.all_indices() {
            let state = match self.lr1_table.states.get(s.get() as usize) {
                Some(s) => s,
                None => break,
            };
            for (symbol, to) in state.transitions.clone() {
                self.compute_state(s, to, symbol)?;
            }
        }
        Ok(())
    }

    fn compute_state(
        &mut self,
        s: StateIdx<LR1>,
        s_prime: StateIdx<LR1>,
        symbol: Symbol,
    ) -> Result<(), Error> {
        let lookaheads_list = self.propagate_lookaheads(s, s_prime);
        let mut found = false;
        let mut isocore = s_prime;
        loop {
            if self.are_compatible(isocore, &lookaheads_list)? {
                found = true;
                break;
            }
            isocore = self.isocores_next[isocore.get() as usize];
            if isocore == s_prime {
                break;
            }
        }
        let state = &self.lr1_table.states[isocore];
        if !found {
            let mut new_state = state.clone();
            for (item, lookaheads) in new_state.core_items_mut().iter_mut().zip(lookaheads_list) {
                item.lookaheads = lookaheads;
            }
            let new_index = self.lr1_table.states.push(new_state)?;
            self.lalr_isocores
                .push(self.lalr_isocores[isocore.get() as usize]);
            self.isocores_next
                .push(self.isocores_next[isocore.get() as usize]);
            self.isocores_next[isocore.get() as usize] = new_index;
            self.lookaheads_recomputed
                .insert(self.isocores_next.len() as StateIdxRaw - 1);
            self.lr1_table.states[s]
                .transitions
                .insert(symbol, new_index);
        } else if !self.lookaheads_recomputed.contains(isocore.get()) {
            for (item, lookaheads) in self.lr1_table.states[isocore]
                .items
                .iter_mut()
                .zip(lookaheads_list)
            {
                item.lookaheads = lookaheads;
            }
            self.lookaheads_recomputed.insert(isocore.get());
        } else {
            self.lr1_table.states[s].transitions.insert(symbol, isocore);
            self.merge_lookaheads(isocore, lookaheads_list)?;
        }
        Ok(())
    }

    fn merge_lookaheads(
        &mut self,
        s: StateIdx<LR1>,
        lookaheads_list: Vec<Set<Lookahead>>,
    ) -> Result<(), Error> {
        let state = &mut self.lr1_table.states[s];
        let mut new_lookaheads = false;
        for (item, lookaheads) in state.core_items_mut().iter_mut().zip(lookaheads_list) {
            let old_size = item.lookaheads.len();
            item.lookaheads.extend(lookaheads);
            if item.lookaheads.len() != old_size {
                new_lookaheads = true;
            }
        }
        if new_lookaheads {
            for (symbol, to) in state.transitions.clone() {
                if !self.lookaheads_recomputed.contains(to.get()) {
                    break;
                }
                self.compute_state(s, to, symbol)?;
            }
        }
        Ok(())
    }

    /// Get the dominant contribution of `annotation` in the context of
    /// `lookahead_sets`.
    ///
    /// # Returns
    /// - `Ok(None)` if `annotation` contributes nothing in the context of
    ///   `lookahead_sets`.
    /// - `Ok(Some(index))` if among the contributions of `annotation` in the context of
    ///   `lookahead_sets`, one was selected as dominant, either because it was the only
    ///   one or because conflict resolution suceeded.
    /// - `Err(conflict)` if multiple contributions were selected, and conflict
    ///   resolution failed.
    fn dominant_contribution<'b>(
        grammar_data: &GrammarData,
        dominant_contribution_alloc: &mut DominantContributionAlloc,
        conflict_resolution_cache: &mut ConflictResolutionCache,
        annotation: &Annotation,
        lookahead_sets: impl Iterator<Item = &'b Set<Lookahead>>,
    ) -> Result<Option<ContributionIndex>, Conflict> {
        let contributions = &annotation.conflict.contributions;
        let possible_contributions = &annotation.possible_contributions;
        let lookahead = annotation.conflict.lookahead;
        let items_lookaheads_contain_t = {
            dominant_contribution_alloc
                .items_lookaheads_contain_t
                .clear();
            for (i, lookaheads) in lookahead_sets.enumerate() {
                if lookaheads.contains(&lookahead) {
                    dominant_contribution_alloc
                        .items_lookaheads_contain_t
                        .insert(i as ItemIdx);
                }
            }
            &dominant_contribution_alloc.items_lookaheads_contain_t
        };
        let conflicting = &mut dominant_contribution_alloc.dominant_contribution_conflicting;
        conflicting.clear();
        if possible_contributions.shift {
            conflicting.push(ContributionIndex::Shift);
        }
        for (i, reduce_contribution) in possible_contributions.reduces.iter().enumerate() {
            // Technically, should be min<ItemIdx, ProdIdxRaw>
            let i = i as ReduceIdx;
            match reduce_contribution {
                Some(bitset) => {
                    if bitset.intersects_with(items_lookaheads_contain_t) {
                        conflicting.push(ContributionIndex::new_reduce(i));
                    }
                }
                None => conflicting.push(ContributionIndex::new_reduce(i)),
            }
        }
        if conflicting.is_empty() {
            Ok(None)
        } else if conflicting.len() == 1 {
            Ok(Some(conflicting[0]))
        } else {
            let dominant_contribution = grammar_data.dominant_contribution(
                &mut dominant_contribution_alloc.solutions_used,
                conflict_resolution_cache,
                contributions,
                conflicting,
                lookahead,
            );
            match dominant_contribution {
                ContributionSelection::Some(index) => Ok(Some(index)),
                _ => Err(Conflict {
                    lookahead,
                    contributions: {
                        let mut result_contributions =
                            crate::output::error::ConflictContributions {
                                has_shift: false,
                                reduces: Vec::new(),
                            };
                        for contribution in conflicting {
                            match contributions.index(*contribution).unwrap() {
                                ShiftOrReduce::Shift => result_contributions.has_shift = true,
                                ShiftOrReduce::Reduce(prod_idx) => {
                                    result_contributions.reduces.push(prod_idx);
                                }
                            }
                        }
                        result_contributions
                    },
                }),
            }
        }
    }
}

impl<'a> Deref for Phase3<'a> {
    type Target = Phase2<'a>;

    fn deref(&self) -> &Self::Target {
        &self.phase2
    }
}

impl<'a> DerefMut for Phase3<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.phase2
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        ielr::phase2::{ConflictRoot, PossibleContributions},
        input::{
            ConflictSolution, ConflictingAction, Grammar,
            Symbol::{Node as N, Token as T},
        },
        output::{error::ConflictContributions, Item, Lookahead},
        test_common::{
            collect,
            nodes::{E, START},
            tokens::{INT, PLUS, TIMES},
        },
    };
    use std::rc::Rc;

    #[test]
    fn dominant_contribution() {
        let mut grammar = Grammar::new();
        let mut productions = Vec::new();
        for (lhs, rhs) in [
            (START, vec![N(E)]),
            (E, vec![N(E), T(PLUS), N(E)]),
            (E, vec![N(E), T(TIMES), N(E)]),
            (E, vec![T(INT)]),
        ] {
            productions.push(grammar.add_production(lhs, rhs).unwrap());
        }
        grammar.add_conflict_solution(ConflictSolution {
            prefer: ConflictingAction::Reduce(productions[1]),
            over: ConflictingAction::Shift(Lookahead::Token(PLUS)),
        });
        // uh-oh: we forgot about E → E × E VS shift(+) !
        let grammar_data = GrammarData::new(&grammar).unwrap();
        let mut conflict_resolution_cache =
            ConflictResolutionCache::new(&grammar_data.conflict_solutions);

        // E + E VS shift(+)
        let items = [
            Item {
                production: productions[1],
                index: 1,
                lookaheads: collect(&[Lookahead::Token(PLUS), Lookahead::Token(TIMES)]),
            },
            Item {
                production: productions[1],
                index: 3,
                lookaheads: collect(&[Lookahead::Token(PLUS), Lookahead::Token(TIMES)]),
            },
        ];
        let annotation = Annotation {
            conflict: Rc::new(ConflictRoot {
                s: StateIdx::new(0),
                lookahead: Lookahead::Token(PLUS),
                contributions: ConflictContributions {
                    has_shift: true,
                    reduces: vec![productions[1]],
                },
            }),
            possible_contributions: PossibleContributions {
                shift: true,
                reduces: vec![Some(collect(&[1]))],
            },
        };

        assert_eq!(
            Phase3::dominant_contribution(
                &grammar_data,
                &mut DominantContributionAlloc::new(&grammar_data),
                &mut conflict_resolution_cache,
                &annotation,
                items.iter().map(|item| &item.lookaheads)
            ),
            Ok(Some(ContributionIndex::new_reduce(0)))
        );

        // E × E VS shift(+)
        let items = [
            Item {
                production: productions[1],
                index: 1,
                lookaheads: collect(&[Lookahead::Token(PLUS), Lookahead::Token(TIMES)]),
            },
            Item {
                production: productions[2],
                index: 3,
                lookaheads: collect(&[Lookahead::Token(PLUS), Lookahead::Token(TIMES)]),
            },
        ];
        let annotation = Annotation {
            conflict: Rc::new(ConflictRoot {
                s: StateIdx::new(0),
                lookahead: Lookahead::Token(PLUS),
                contributions: ConflictContributions {
                    has_shift: true,
                    reduces: vec![productions[2]],
                },
            }),
            possible_contributions: PossibleContributions {
                shift: true,
                reduces: vec![Some(collect(&[1]))],
            },
        };

        assert_eq!(
            Phase3::dominant_contribution(
                &grammar_data,
                &mut DominantContributionAlloc::new(&grammar_data),
                &mut conflict_resolution_cache,
                &annotation,
                items.iter().map(|item| &item.lookaheads),
            ),
            Err(Conflict {
                lookahead: Lookahead::Token(PLUS),
                contributions: ConflictContributions {
                    has_shift: true,
                    reduces: vec![productions[2]]
                }
            })
        );
    }
}
