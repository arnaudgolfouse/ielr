/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

//! Phase 4: recompute lookaheads
//!
//! This phase recomputes the lookahead informations for the newly created IELR table,
//! using the same method as phases 0 and 1.

use super::{phase0::GotoIdxVec, phase1::Predecessors, Phase0, Phase1, Phase3};
use crate::{
    lr::{LRState, LRTable, LR1},
    output::Lookahead,
    structures::Set,
    GotoIdx, ItemIdx,
};

pub(crate) struct Phase4 {
    pub(crate) lr1_table: LRTable<LR1>,
    gotos_in_state: Vec<Vec<GotoIdx>>,
    goto_follows: GotoIdxVec<Set<Lookahead>>,
    predecessors: Vec<Predecessors<LR1>>,
}

impl Phase4 {
    pub(crate) fn new(phase3: Phase3) -> Self {
        let (goto_data, gotos_in_state) = Phase0::compute_goto_idx_tables(&phase3.lr1_table);
        let (gf_i, gf_p) = Phase0::compute_gf_i_p(
            &phase3.lr1_table,
            phase3.grammar_data,
            &goto_data,
            &gotos_in_state,
        );
        let successor_follows = GotoIdxVec(Phase0::compute_successor_follows(
            phase3.grammar_data,
            &phase3.lr1_table,
            &goto_data,
            &gotos_in_state,
        ));
        let goto_follows = GotoIdxVec(Phase0::compute_goto_follows(
            &gf_i,
            &gf_p,
            &successor_follows,
        ));
        let predecessors = Phase1::compute_predecessors(&phase3.lr1_table);
        Self {
            lr1_table: phase3.lr1_table,
            gotos_in_state,
            goto_follows,
            predecessors,
        }
    }

    pub(crate) fn compute_lookaheads(&mut self) {
        // clear lookaheads, so we compute everything.
        for state in &mut self.lr1_table.states as &mut [LRState<_>] {
            for item in &mut state.items {
                item.lookaheads.clear();
            }
        }
        for state_index in self.lr1_table.states.all_indices() {
            let state = &self.lr1_table.states[state_index];
            for item_index in 0..state.items.len() as ItemIdx {
                Phase1::compute_lookaheads_item(
                    &mut self.lr1_table,
                    &self.gotos_in_state,
                    &self.goto_follows,
                    &self.predecessors,
                    state_index,
                    item_index,
                );
            }
        }
    }
}
