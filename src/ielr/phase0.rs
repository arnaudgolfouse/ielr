/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

//! Phase 0: LALR(1)
//!
//! 1. Compute LR(0) parser tables.
//! 2. Compute reduction lookahead sets using the technique described by
//!    [DeRemer and Pennello](<https://hassan-ait-kaci.net/pdf/others/p615-deremer.pdf>).

use super::item_relation::GotoRelation;
use crate::{
    grammar_data::{ConflictResolutionCache, GrammarData},
    input::{Node, ProdIdx, Symbol},
    lr::{LRTable, LALR},
    output::{Lookahead, Statistics},
    structures::{Map, Set},
    GotoIdx, ItemIdx, StateIdx,
};

/// Wrapper around [`Vec<T>`].
///
/// Only indexable by [`GotoIdx`].
#[derive(Clone, Debug)]
pub(crate) struct GotoIdxVec<T>(pub(super) Vec<T>);

impl<T> std::ops::Index<GotoIdx> for GotoIdxVec<T> {
    type Output = T;

    fn index(&self, index: GotoIdx) -> &Self::Output {
        &self.0[index as usize]
    }
}

impl<T> std::ops::Deref for GotoIdxVec<T> {
    type Target = [T];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<'a, T> IntoIterator for &'a GotoIdxVec<T> {
    type Item = &'a T;
    type IntoIter = <&'a [T] as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        (self as &[T]).iter()
    }
}

/// Various data related to [`GotoIdx`]s.
pub(crate) struct GotoIndicesData<Kind> {
    /// Maps a [`GotoIdx`] to its starting state.
    pub(crate) from_state: GotoIdxVec<StateIdx<Kind>>,
    /// Maps a [`GotoIdx`] `g` to the state obtained by
    /// - starting at `from_state[g]`
    /// - taking the transition on the left-hand side of `g`.
    pub(crate) to_state: GotoIdxVec<StateIdx<Kind>>,
    /// Maps a [`GotoIdx`] to its left-hand side symbol.
    pub(crate) symbol: GotoIdxVec<Node>,
    /// Maps a [`GotoIdx`] to its productions.
    ///
    /// These are the productions of the items in [`Self::local_indices`].
    pub(crate) productions: GotoIdxVec<Vec<ProdIdx>>,
    /// Maps a [`GotoIdx`] to its items indices in its [state](Self::from_state).
    ///
    /// The associated item are the ones with the `GotoIdx`'s [symbol](Self::symbol) as
    /// their left-hand side.
    pub(crate) local_indices: GotoIdxVec<Vec<ItemIdx>>,
}

impl<Kind> GotoIndicesData<Kind> {
    pub(crate) fn get_all_goto_indices(&self) -> impl Iterator<Item = GotoIdx> + '_ {
        0..self.from_state.len() as GotoIdx
    }
}

pub(crate) struct Phase0<'a> {
    pub(crate) grammar_data: &'a GrammarData<'a>,
    pub(crate) conflict_resolution_cache: ConflictResolutionCache,
    /// The LALR table.
    ///
    /// Lookaheads will be computed at the end of phase 1.
    pub(crate) lalr_table: &'a mut LRTable<LALR>,
    pub(crate) statistics: Statistics,
    /// Various data about gotos in the LR(0) table.
    pub(crate) goto_data: GotoIndicesData<LALR>,
    /// Holds all gotos indices for each non-core items of each state.
    ///
    /// Avoid searching in all the items for a specific state.
    ///
    /// # Example
    /// If `s` is a state index, and `i` a (non-core) item index in s, we can get the
    /// goto associated with `i` with `gotos_in_state[s][i]`.q
    ///
    /// # Warning
    /// This field should **not** be used in itself! Instead, use `Phase0::get_goto`.
    pub(super) gotos_in_state: Vec<Vec<GotoIdx>>,
    /// Goto Follows internal
    ///
    /// If `i` and `i'` are related, then there is an **internal dependency**
    /// between `i` and `i'`.
    ///
    /// `(g, g')` is in this set if:
    ///
    /// 1. there exists `β ∈ Node*` and a production
    ///    `l → goto_data.symbol[g] .. β ∈ goto_data.productions[g']`
    /// 2. `from_state[g] == from_state[g']`
    /// 3. `β ⇒* ε`
    ///
    /// # Example
    /// For the grammar
    /// ```text
    /// S → a A
    /// A → B C
    /// B → D b
    /// C →
    /// D → d
    /// ```
    /// And the state
    /// ```text
    /// S → a∙A
    /// A →∙B C    (g₁)  ◂┐ inherits lookaheads
    /// B →∙D b    (g₂)  ─┘ ◂┐ nope: only contains b
    /// D →∙d      (g₃)     ─┘
    /// ```
    /// Then `GFᵢ` contains `(g₂, g₁)` but not `(g₃, g₂)`.
    pub(crate) gf_i: GotoRelation,
    /// If `i` is the index of an item in `goto_data.local_indices[g]`, then for all
    /// item `i'` such that `i ∈ i'.directly_derives`, `successor_follows[g]` includes
    /// `{ t ∈ T ∣ i'[1..] ⇒* tα }`.
    pub(crate) successor_follows: GotoIdxVec<Set<Lookahead>>,
    /// Map a [`GotoIdx`] to the set of lookaheads that may appear after parsing one of
    /// the items associated with this goto.
    ///
    /// ```text
    ///    goto_follows[g] = { l ∈ L ∣ ∃i', GFₛᵢₚ*(g, g') ∧ ∃δ(Σ[to_state[g']], l) }
    /// OR goto_follows[g] = { l ∈ L ∣ ∃i', GFᵢₚ*(g, g') ∧ l ∈ successor_follows[g'] }
    /// OR goto_follows[g] = { l ∈ L ∣ ∃i', GFᵢₚ*(g, g') ∧ l ∈ always_follows[g'] }
    /// ```
    pub(crate) goto_follows: GotoIdxVec<Set<Lookahead>>,
}

impl<'a> Phase0<'a> {
    /// Compute the phase 0 of the IELR algorithm.
    ///
    /// # Input
    /// - `grammar_data`: user-provided grammar (+ some computation, like nullable
    ///   nodes)
    /// - `table`: the LALR states table.
    pub(crate) fn new(grammar_data: &'a GrammarData, lalr_table: &'a mut LRTable<LALR>) -> Self {
        let conflicting_actions = ConflictResolutionCache::new(&grammar_data.conflict_solutions);
        let statistics = Statistics {
            lalr_states_count: lalr_table.states.len().get() as usize,
            lr1_states_count: 0,
            conflict_resolution_used: Vec::new(),
            algorithm_needed: crate::Algorithm::default(),
        };
        let (goto_data, gotos_in_state) = Self::compute_goto_idx_tables(lalr_table);
        let (gf_i, gf_p) =
            Self::compute_gf_i_p(lalr_table, grammar_data, &goto_data, &gotos_in_state);
        let successor_follows = GotoIdxVec(Self::compute_successor_follows(
            grammar_data,
            lalr_table,
            &goto_data,
            &gotos_in_state,
        ));
        let goto_follows = GotoIdxVec(Self::compute_goto_follows(&gf_i, &gf_p, &successor_follows));
        Self {
            grammar_data,
            conflict_resolution_cache: conflicting_actions,
            lalr_table,
            statistics,
            goto_data,
            gotos_in_state,
            gf_i,
            successor_follows,
            goto_follows,
        }
    }

    /// Get the accumulated statistics.
    pub(crate) fn get_statistics(&mut self) -> Statistics {
        self.statistics.clone()
    }

    /// Get all [`GotoIdx`] whose [`from_state`](GotoIndicesData::from_state) is `s`.
    pub(crate) fn get_gotos_in_state(
        &self,
        s: StateIdx<LALR>,
    ) -> impl Iterator<Item = GotoIdx> + '_ {
        self.gotos_in_state[s.get() as usize].iter().copied()
    }

    /// This method should be used to access [`Self::gotos_in_state`].
    pub(super) fn get_goto<Kind>(
        table: &LRTable<Kind>,
        gotos_in_state: &[Vec<GotoIdx>],
        s: StateIdx<Kind>,
        item_index: ItemIdx,
    ) -> GotoIdx {
        let state = &table.states[s];
        let core_items_len = state.core_items_len;
        // We want `core_items_len` to be the number of items with index > 0, which is
        // **not** core_items_len if the state is a starting state.
        let index = item_index - core_items_len;
        let gotos_in_state = &gotos_in_state[s.get() as usize];
        gotos_in_state[index as usize]
    }

    /// Compute basic information related to [`GotoIdx`]s of the `table`:
    /// - The state that contains the non-core item.
    /// - The state obtained by following the left-hand of the item.
    /// - The left-hand side of the item’s production.
    /// - The item’s production.
    pub(super) fn compute_goto_idx_tables<Kind>(
        table: &LRTable<Kind>,
    ) -> (GotoIndicesData<Kind>, Vec<Vec<GotoIdx>>) {
        let mut from_state = Vec::new();
        let mut to_state = Vec::new();
        let mut goto_symbol = Vec::new();
        let mut goto_productions: Vec<Vec<_>> = Vec::new();
        let mut local_indices: Vec<Vec<_>> = Vec::new();
        let mut gotos_in_state = Vec::new();
        for (state_idx, state) in table.states.all_indices().zip(table.states.iter()) {
            let mut non_core_items_indices = Vec::new();

            // CONDITION FOR BEING GOTO-BASED:
            // s:
            //   A → a∙B             ─┐
            //   A → a∙C [precedence] │─┐
            //   B →∙b₁              ◂┘ │
            //   B →∙b₂              ◂┘ │
            //   C →∙c₁                ◂┘
            //   C →∙c₂                ×┘
            // Here, precedence does not influence B, but it does C.
            // In other words, all B → ... items have the same reverse/forward
            // relations, but not C.
            // Thus the condition for a goto with lhs B to _not_ be split up into items is :
            // - No precedence annotations on B → ... items.
            // - No items forbid a transition to a B → ... item.

            // Which goto *must* be split up into individual items.
            let mut nodes_no_goto: Set<Node> = Set::default();
            if state.uses_precedence {
                for item in state.all_items() {
                    if item.index == 0 && !item.annotations.is_empty() {
                        nodes_no_goto.insert(item.prod_idx.lhs);
                    }
                    nodes_no_goto.extend(&item.forbidden_nodes);
                }
            }
            let mut gotos: Map<Node, GotoIdx> = Map::default();

            for (index, item) in state.all_items().iter().enumerate() {
                // This rather than skipping core items. That way we still process starting states.
                if item.index > 0 {
                    continue;
                }
                if !nodes_no_goto.contains(&item.prod_idx.lhs) {
                    if let Some(&i) = gotos.get(&item.prod_idx.lhs) {
                        goto_productions[i as usize].push(item.prod_idx);
                        local_indices[i as usize].push(index as ItemIdx);
                        non_core_items_indices.push(i);
                        continue;
                    } else {
                        gotos.insert(item.prod_idx.lhs, from_state.len() as GotoIdx);
                    }
                }
                let goto_index = from_state.len() as GotoIdx;
                non_core_items_indices.push(goto_index);
                from_state.push(state_idx);
                to_state.push(state.transitions[&Symbol::Node(item.prod_idx.lhs)]);
                goto_symbol.push(item.prod_idx.lhs);
                goto_productions.push(vec![item.prod_idx]);
                local_indices.push(vec![index as ItemIdx]);
            }
            gotos_in_state.push(non_core_items_indices);
        }
        if from_state.len() >= ItemIdx::MAX as usize {
            panic!(
                "internal overflow: the number of non-core items is over {}",
                ItemIdx::MAX
            )
        }
        (
            GotoIndicesData {
                from_state: GotoIdxVec(from_state),
                to_state: GotoIdxVec(to_state),
                symbol: GotoIdxVec(goto_symbol),
                productions: GotoIdxVec(goto_productions),
                local_indices: GotoIdxVec(local_indices),
            },
            gotos_in_state,
        )
    }

    /// Computes [`Self::gf_i`] and `gf_p`.
    ///
    /// # Example
    /// For the grammar
    /// ```text
    /// S → a A
    /// A → a B C
    /// B → b
    /// C →
    /// ```
    /// And the states
    /// ```text
    /// s₁:
    ///   S → a∙A
    ///   A →∙a B C    (g₁) ◂┐
    /// s₂:                  │ inherits lookaheads because C is nullable
    ///   A → a∙B C          │
    ///   B →∙b        (g₂) ─┘
    /// ```
    /// Then `GFₚ` contains `(g₂, g₁)`.
    pub(super) fn compute_gf_i_p<Kind>(
        table: &LRTable<Kind>,
        grammar_data: &GrammarData,
        goto_data: &GotoIndicesData<Kind>,
        gotos_in_state: &[Vec<GotoIdx>],
    ) -> (GotoRelation, GotoRelation) {
        let mut gf_i = GotoRelation::new(goto_data.from_state.len() as GotoIdx);
        let mut gf_p = GotoRelation::new(goto_data.from_state.len() as GotoIdx);
        // In the above example:
        //     s₁            [A → a B C]  [1]              g₁
        for ((&state_prime, (prods_prime, local_indices)), g_prime) in goto_data
            .from_state
            .iter()
            .zip(goto_data.productions.iter().zip(&goto_data.local_indices))
            .zip(goto_data.get_all_goto_indices())
        {
            // For example rhs = a B C, local_index_prime = 1
            for (rhs, local_index_prime) in prods_prime
                .iter()
                .map(|prod| grammar_data.get_rhs(*prod).unwrap_or_default())
                .zip(local_indices.iter().copied())
            {
                // In 0..=rhs.len()
                let nullables_symbols_at_end = {
                    let mut nullables = 0;
                    for symbol in rhs.iter().rev() {
                        match symbol {
                            Symbol::Node(n) if grammar_data.nullables.is_nullable(*n) => {
                                nullables += 1;
                            }
                            _ => break,
                        }
                    }
                    nullables
                };
                // For example
                // - ~~index = 0, symbol = a, state_idx = s₁~~ skipped
                // - index = 1, symbol = B, state_idx = s₂, item = A → a∙B C
                // - etc...
                for (index, (symbol, state_idx, state, item)) in table
                    .follow_transitions(state_prime, local_index_prime, rhs)
                    .enumerate()
                    // Only consider `index` such that rhs[index..] is nullable.
                    .skip((rhs.len() - nullables_symbols_at_end).saturating_sub(1))
                {
                    for &g in gotos_in_state[state_idx.get() as usize]
                        .iter()
                        .filter(|&&g| Symbol::Node(goto_data.symbol[g]) == symbol)
                    {
                        if state.uses_precedence {
                            for &local_index in &goto_data.local_indices[g] {
                                if item.directly_derives.contains(local_index) {
                                    if index == 0 {
                                        gf_i.add(g, g_prime);
                                    } else {
                                        gf_p.add(g, g_prime);
                                    }
                                    break;
                                }
                            }
                        }
                        // If the receiving state does not use precedence, then
                        // `A → a∙B C` *must* derive any `B →∙b` rule.
                        else if index == 0 {
                            gf_i.add(g, g_prime);
                        } else {
                            gf_p.add(g, g_prime);
                        }
                    }
                }
            }
        }
        (gf_i, gf_p)
    }

    /// Compute [`Self::successor_follows`].
    ///
    /// If `i` is the index of an item in `goto_data.local_indices[g]`, then for all
    /// items `i'` such that `i ∈ i'.directly_derives`, `successor_follows[g]`
    /// is `{ t ∈ T ∣ i'[1..] ⇒* tα }`.
    pub(super) fn compute_successor_follows<Kind>(
        grammar_data: &GrammarData,
        table: &LRTable<Kind>,
        goto_data: &GotoIndicesData<Kind>,
        gotos_in_state: &[Vec<GotoIdx>],
    ) -> Vec<Set<Lookahead>> {
        let mut successor_follows = vec![Set::default(); goto_data.to_state.len()];
        // Special handling for starting states
        for (&start_node, &start_state_idx) in &table.starting_states {
            let start_state = &table.states[start_state_idx];
            for (item_idx, item) in start_state.items.iter().enumerate() {
                if item.prod_idx.lhs == start_node {
                    let g: GotoIdx = Phase0::get_goto(
                        table,
                        gotos_in_state,
                        start_state_idx,
                        item_idx as ItemIdx,
                    );
                    successor_follows[g as usize].insert(Lookahead::Eof);
                }
            }
        }
        for (state_idx, state) in table.states.all_indices().zip(table.states.iter()) {
            for item in &state.items {
                let after_dot =
                    &grammar_data.get_rhs(item.prod_idx).unwrap_or_default()[item.index as usize..];
                if let Some((Symbol::Node(_), to_derive)) = after_dot.split_first() {
                    let derived = grammar_data.derived_tokens(to_derive);
                    for index in item.directly_derives.iter() {
                        let g: GotoIdx =
                            Phase0::get_goto(table, gotos_in_state, state_idx, index as ItemIdx);
                        successor_follows[g as usize]
                            .extend(derived.iter().map(|t| Lookahead::Token(*t)));
                    }
                }
            }
        }
        successor_follows
    }

    /// `goto_follows[g] = { l ∈ L ∣ ∃g', GFᵢₚ*(g, g') ∧ l ∈ successor_follows[g'] }`
    // FIXME(perf): with precedence, execution time is largely dominated by this function (~65%, not mesured scientifically). In particular, ~50% of total time is spent computing the closure.
    //       This is mostly due to the (much) higher number of GotoIdx when using precedence: we should find a way to reduce this.
    pub(super) fn compute_goto_follows(
        gf_i: &GotoRelation,
        gf_p: &GotoRelation,
        successor_follows: &[Set<Lookahead>],
    ) -> Vec<Set<Lookahead>> {
        let mut gf_ip = GotoRelation::new(successor_follows.len() as GotoIdx);
        for (g1, g2) in gf_i.get_all_relations().chain(gf_p.get_all_relations()) {
            gf_ip.add(g1, g2);
        }
        let gf_ip_closure = gf_ip.closure();

        let mut goto_follows = Vec::new();
        for g in 0..successor_follows.len() as GotoIdx {
            let mut follows = Set::default();
            for g_prime in gf_ip_closure.get_relations_from(g) {
                follows.extend(&successor_follows[g_prime as usize]);
            }
            goto_follows.push(follows);
        }
        goto_follows
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        ielr::tests::*,
        input::{Grammar, Symbol::Node as N, Symbol::Token as T},
        structures::Set,
        test_common::{collect, expressions},
    };

    /// Tests the computation of `GFₛ`, and `successor_follows`.
    #[test]
    fn successor_follows() {
        // boilerplate
        let grammar = grammar_1();
        let grammar_data = GrammarData::new(&grammar).unwrap();
        let lalr_table = LRTable::new_lr0(&grammar_data, collect(&[START])).unwrap();
        let (goto_data, gotos_in_state) = Phase0::compute_goto_idx_tables(&lalr_table);

        // All gotos in the table.
        get_gotos! {in &lalr_table;
                        => S0 {START: g0_start};
            S0 => T(T1) => S1 {N1: g1_n1};
            S0 => T(T2) => S2 {N4: g2_n4, N5: g2_n5};
            S1 => N(N1) => S3 {N2: g3_n2, N3: g3_n3};
            S2 => N(N5) => S6 {N3: g6_n3};
            S3 => N(N2) => S8 {N6: g8_n6};
        }

        let successor_follows = Phase0::compute_successor_follows(
            &grammar_data,
            &lalr_table,
            &goto_data,
            &gotos_in_state,
        );

        // check successor_follows
        use Lookahead::Token as LT;
        assert_eq!(
            successor_follows[g1_n1 as usize],
            collect(&[LT(T1), LT(T3), LT(T4)])
        );
        assert_eq!(successor_follows[g2_n4 as usize], collect(&[]));
        assert_eq!(successor_follows[g2_n5 as usize], collect(&[LT(T3)]));
        assert_eq!(
            successor_follows[g3_n2 as usize],
            collect(&[LT(T1), LT(T4)])
        );
        assert_eq!(successor_follows[g3_n3 as usize], collect(&[]));
        assert_eq!(successor_follows[g6_n3 as usize], collect(&[]));
        assert_eq!(successor_follows[g8_n6 as usize], collect(&[]));
    }

    /// Tests the computation of `GFₛ`, and `successor_follows`.
    #[test]
    fn successor_follows2() {
        // boilerplate
        let grammar = grammar_2();
        let grammar_data = GrammarData::new(&grammar).unwrap();
        let lr0_table = LRTable::new_lr0(&grammar_data, collect(&[START])).unwrap();
        let (goto_data, gotos_in_state) = Phase0::compute_goto_idx_tables(&lr0_table);

        // All gotos in the table.
        get_gotos! {in &lr0_table;
                        => S0 {START: g0_start};
            S0 => T(T1) => S1 {N1: g1_n1, N3: g1_n3, N7: g1_n7, N8: g1_n8};
            S1 => N(N1) => S2 {N2: g2_n2};
            S2 => N(N2) => S3 {N6: g3_n6, N7: g3_n7, N8: g3_n8};
            S1 => N(N8) => S7 {N3: g7_n3, N7: g7_n7, N8: g7_n8};
            S2 => T(T2) => S9 {N4: g9_n4, N6: g9_n6, N7: g9_n7, N8: g9_n8};
            S9 => N(N4) => S10 {N5: g10_n5};
        }

        let successor_follows = Phase0::compute_successor_follows(
            &grammar_data,
            &lr0_table,
            &goto_data,
            &gotos_in_state,
        );

        // check successor_follows
        use Lookahead::Token as LT;
        assert_eq!(successor_follows[g1_n1 as usize], collect(&[LT(T2)]));
        assert_eq!(successor_follows[g1_n3 as usize], collect(&[]));
        assert_eq!(successor_follows[g1_n7 as usize], collect(&[]));
        assert_eq!(
            successor_follows[g1_n8 as usize],
            collect(&[LT(T1), LT(T3)])
        );
        assert_eq!(
            successor_follows[g2_n2 as usize],
            collect(&[LT(T1), LT(T3)])
        );
        assert_eq!(successor_follows[g3_n6 as usize], collect(&[]));
        assert_eq!(successor_follows[g3_n7 as usize], collect(&[]));
        assert_eq!(
            successor_follows[g3_n8 as usize],
            collect(&[LT(T1), LT(T3)])
        );
        assert_eq!(successor_follows[g7_n3 as usize], collect(&[]));
        assert_eq!(successor_follows[g7_n7 as usize], collect(&[]));
        assert_eq!(
            successor_follows[g7_n8 as usize],
            collect(&[LT(T1), LT(T3)])
        );
        assert_eq!(successor_follows[g9_n4 as usize], collect(&[LT(T5)]));
        assert_eq!(successor_follows[g9_n6 as usize], collect(&[]));
        assert_eq!(successor_follows[g9_n7 as usize], collect(&[]));
        assert_eq!(
            successor_follows[g9_n8 as usize],
            collect(&[LT(T1), LT(T3)])
        );
        assert_eq!(successor_follows[g10_n5 as usize], collect(&[]));
    }

    #[test]
    fn gotos_follows_internal() {
        // boilerplate
        let grammar = grammar_2();
        let grammar_data = GrammarData::new(&grammar).unwrap();
        let lalr_table = LRTable::new_lr0(&grammar_data, collect(&[START])).unwrap();
        let (goto_data, gotos_in_state) = Phase0::compute_goto_idx_tables(&lalr_table);
        let (gf_i, _) =
            Phase0::compute_gf_i_p(&lalr_table, &grammar_data, &goto_data, &gotos_in_state);
        // All gotos in the table.
        get_gotos! {in &lalr_table;
                        => S0 {START: g0_start};
            S0 => T(T1) => S1 {N1: g1_n1, N3: g1_n3, N7: g1_n7, N8: g1_n8};
            S1 => N(N1) => S2 {N2: g2_n2};
            S2 => N(N2) => S3 {N6: g3_n6, N7: g3_n7, N8: g3_n8};
            S1 => N(N8) => S7 {N3: g7_n3, N7: g7_n7, N8: g7_n8};
            S2 => T(T2) => S9 {N4: g9_n4, N6: g9_n6, N7: g9_n7, N8: g9_n8};
            S9 => N(N4) => S10 {N5: g10_n5};
        }

        // check gf_i relation
        assert_eq!(
            gf_i.get_all_relations().collect::<Set<_>>(),
            [
                (g1_n3, g1_n1),
                (g1_n7, g1_n3),
                (g1_n8, g1_n7),
                (g3_n7, g3_n6),
                (g3_n8, g3_n7),
                (g7_n7, g7_n3),
                (g7_n8, g7_n7),
                (g9_n7, g9_n6),
                (g9_n8, g9_n7),
            ]
            .into_iter()
            .collect::<Set<_>>()
        );
    }

    #[test]
    fn gotos_follows_internal_precedence() {
        use expressions::symbols::{E, PLUS};
        let (grammar, [add_prod, _, _, _, if_prod]) = expressions::get_grammar_with_precedence();

        let grammar_data = GrammarData::new(&grammar).unwrap();
        let lalr_table = LRTable::new_lr0(&grammar_data, collect(&[E])).unwrap();
        let (goto_data, gotos_in_state) = Phase0::compute_goto_idx_tables(&lalr_table);
        let (gf_i, gf_p) =
            Phase0::compute_gf_i_p(&lalr_table, &grammar_data, &goto_data, &gotos_in_state);

        // E →∙E + E [_, _]
        // E →∙E + E [_, 2]
        // ...
        let state1_idx = lalr_table.starting_states[&E];
        let state1 = &lalr_table.states[state1_idx];

        // E → E +∙E [_, _]
        // E → E +∙E [_, 2]
        // E →∙if E  [_, _]
        // ...
        let state2_idx = lalr_table.states[state1.transitions[&N(E)]].transitions[&T(PLUS)];
        let state2 = &lalr_table.states[state2_idx];

        let plus_item_no_annotations_index = {
            state1
                .items
                .iter()
                .enumerate()
                .find(|(_, item)| {
                    item.prod_idx == add_prod
                        && item.index == 0
                        && item.annotations.forbidden_left.is_empty()
                        && item.annotations.forbidden_right.is_empty()
                })
                .unwrap()
                .0 as ItemIdx
        };
        let plus_item_annotations_index = {
            state1
                .items
                .iter()
                .enumerate()
                .find(|(_, item)| {
                    item.prod_idx == add_prod
                        && item.index == 0
                        && item.annotations.forbidden_left.is_empty()
                        && item.annotations.forbidden_right.first() == Some(&Some(2))
                })
                .unwrap()
                .0 as ItemIdx
        };

        let if_item_index = {
            state2
                .items
                .iter()
                .enumerate()
                .find(|(_, item)| {
                    item.prod_idx == if_prod
                        && item.index == 0
                        && item.annotations.forbidden_left.is_empty()
                        && item.annotations.forbidden_right.is_empty()
                })
                .unwrap()
                .0 as ItemIdx
        };

        // E →∙E + E [_, _]
        // E →∙E + E [_, 2]
        // E →∙if E  [_, _]
        let (goto1, goto2, goto3) = {
            let find_goto = |state_idx: StateIdx<_>, index: ItemIdx| {
                goto_data
                    .from_state
                    .iter()
                    .zip(&goto_data.local_indices)
                    .zip(goto_data.get_all_goto_indices())
                    .find(|((from_state, indices), _)| {
                        **from_state == state_idx && indices.len() == 1 && indices[0] == index
                    })
                    .unwrap()
                    .1
            };
            (
                find_goto(state1_idx, plus_item_no_annotations_index),
                find_goto(state1_idx, plus_item_annotations_index),
                find_goto(state2_idx, if_item_index),
            )
        };

        assert!(gf_i.get_all_relations().count() == 0);
        assert!(gf_p.contains(goto3, goto1));
        assert!(!gf_p.contains(goto3, goto2));
    }

    #[test]
    fn number_of_goto_items() {
        use crate::test_common::expressions::symbols::*;

        let mut grammar = Grammar::new();

        // The grammar of (almost) rust expressions.
        grammar.add_production(START, vec![N(E)]).unwrap();
        grammar.add_production(E, vec![T(INT)]).unwrap();
        let operators = [
            (ASSIGN, Some(1), Some(2)),
            (ASSIGN_ADD, Some(1), Some(2)),
            (ASSIGN_SUB, Some(1), Some(2)),
            (ASSIGN_MUL, Some(1), Some(2)),
            (ASSIGN_DIV, Some(1), Some(2)),
            (ASSIGN_MOD, Some(1), Some(2)),
            (ASSIGN_BITAND, Some(1), Some(2)),
            (ASSIGN_BITOR, Some(1), Some(2)),
            (ASSIGN_BITXOR, Some(1), Some(2)),
            (AND, Some(3), Some(4)),
            (OR, Some(3), Some(4)),
            (XOR, Some(3), Some(4)),
            (BITAND, Some(5), Some(6)),
            (BITOR, Some(5), Some(6)),
            (BITXOR, Some(5), Some(6)),
            (EQUAL, Some(7), Some(8)),
            (NEQUAL, Some(7), Some(8)),
            (MORE, Some(9), Some(10)),
            (LESS, Some(9), Some(10)),
            (MOREEQ, Some(9), Some(10)),
            (LESSEQ, Some(9), Some(10)),
            (RANGE, Some(11), Some(12)),
            (OPEN_RANGE, Some(11), Some(12)),
            (LSHIFT, Some(13), Some(14)),
            (RSHIFT, Some(13), Some(14)),
            (PLUS, Some(15), Some(16)),
            (MINUS, Some(15), Some(16)),
            (TIMES, Some(17), Some(18)),
            (DIV, Some(17), Some(18)),
            (INDEX, Some(19), None),
            (CALL, Some(19), None),
            (MINUS, None, Some(20)),
            (QUESTION, Some(21), None),
        ];
        let family = grammar.add_precedence_family();

        for (operator, left_prec, right_prec) in operators {
            let mut rhs = Vec::new();
            if left_prec.is_some() {
                rhs.push(N(E));
            }
            rhs.push(T(operator));
            if right_prec.is_some() {
                rhs.push(N(E));
            }
            let prod = grammar.add_production(E, rhs).unwrap();
            let prod = grammar.get_production_mut(prod).unwrap();
            if let Some(l) = left_prec {
                prod.set_left_precedence(family, l);
            }
            if let Some(r) = right_prec {
                prod.set_right_precedence(family, r);
            }
        }

        let grammar_data = GrammarData::new(&grammar).unwrap();
        let lalr_table = LRTable::new_lr0(&grammar_data, collect(&[E])).unwrap();
        let (_, gotos_in_state) = Phase0::compute_goto_idx_tables(&lalr_table);
        let number_of_gotos: usize = gotos_in_state.iter().map(Vec::len).sum();

        // In short, getting this number down is good.
        assert_eq!(number_of_gotos, 3039);
    }
}
