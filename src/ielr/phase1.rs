/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

//! Phase 1: Compute auxiliary tables
//!
//! This computes auxiliary data, that will be used in phase 2.

use super::{item_relation::GotoRelation, phase0::GotoIdxVec, Phase0};
use crate::{
    input::Symbol,
    lr::{LRTable, LALR},
    output::Lookahead,
    structures::{BitSet, Map, Set},
    GotoIdx, ItemIdx, StateIdx,
};
use std::ops::{Deref, DerefMut};

/// The predecessors of a state.
///
/// Maps each predecessor to the indices of the predecessors of each non-core item.
pub(crate) type Predecessors<Kind> = Map<StateIdx<Kind>, Vec<ItemIdx>>;

pub(crate) struct Phase1<'a> {
    pub(crate) phase0: Phase0<'a>,
    /// Map a state to the set of its predecessors.
    pub(crate) predecessors: Vec<Predecessors<LALR>>,
    /// Map a [`GotoIdx`] `g` to the core items in the same state (`from_state[g]`),
    /// that give their lookaheads to `g`.
    ///
    /// Let `k` be an index in the core items of `from_state[g]`, such that
    /// `from_state[g].items[k] = (l → r, d)` (`d > 0`).
    ///
    /// Then we have `k ∈ follow_kernel_items[g]` if
    /// 1. `∃g', GF*ᵢ(g, g')` and `r[d] == goto_data.symbol[g']`
    ///    
    ///    (if precedence) and `from_state[g].items[k].derives.contains(g')`.
    /// 2. `r[d+1 .. r.len()] ⇒* ε`.
    pub(crate) follow_kernel_items: GotoIdxVec<BitSet<ItemIdx>>,
    /// Map a [`GotoIdx`] `g` to a set of lookaheads.
    ///
    /// `always_follows[g] = { t ∈ L ∣ ∃i', GFᵢ*(g, g') ∧ t ∈ successor_follows[g'] }`
    ///
    /// # Note
    /// On page 961, another approach is outlined, yielding
    /// `always_follows[g] = { t ∈ L ∣ ∃i', GFₛᵢ*(g, g') ∧ ∃δ(Σ[to_state[g']], t) }`
    ///
    /// This could allow us to compute one less closure, by computing `goto_follows`
    /// _after_ this.
    pub(crate) always_follows: GotoIdxVec<Set<Lookahead>>,
}

impl<'a> Phase1<'a> {
    /// Compute the phase 0 of the IELR algorithm.
    pub(crate) fn new(phase0: Phase0<'a>) -> Self {
        let gf_i_closure = phase0.gf_i.closure();

        let mut this = Self {
            predecessors: Self::compute_predecessors(phase0.lalr_table),
            follow_kernel_items: Self::compute_follow_kernel_items(&phase0, &gf_i_closure),
            always_follows: Self::compute_always_follows(&phase0, &gf_i_closure),
            phase0,
        };
        this.compute_lookaheads();
        this
    }

    /// Computes the predecessors of each state.
    pub(super) fn compute_predecessors<Kind>(table: &LRTable<Kind>) -> Vec<Predecessors<Kind>> {
        let mut predecessors = vec![Map::default(); table.states.len().get() as usize];
        for (from, state) in table.states.all_indices().zip(table.states.iter()) {
            for &to in state.transitions.values() {
                let to_state = &table.states[to];
                let mut item_indices = Vec::new();
                let mut items = to_state.items.iter().peekable();
                // Use the fact that items are ordered.
                for (item_index, item) in state.items.iter().enumerate() {
                    if let Some(next) = items.peek() {
                        if next.prod_idx == item.prod_idx && next.index == item.index + 1 {
                            items.next();
                            item_indices.push(item_index as ItemIdx);
                        }
                    }
                }
                predecessors[to.get() as usize].insert(from, item_indices);
            }
        }
        predecessors
    }

    /// Computes [`Self::follow_kernel_items`].
    fn compute_follow_kernel_items(
        phase0: &Phase0,
        gf_i_closure: &GotoRelation,
    ) -> GotoIdxVec<BitSet<ItemIdx>> {
        let mut follow_kernel_items = Vec::new();

        for (from_state_index, goto) in phase0
            .goto_data
            .from_state
            .iter()
            .copied()
            .zip(phase0.goto_data.get_all_goto_indices())
        {
            let mut set = BitSet::new();
            let from_state = &phase0.lalr_table.states[from_state_index];
            for (k, item) in from_state.core_items().iter().enumerate() {
                let rhs = phase0
                    .grammar_data
                    .get_rhs(item.prod_idx)
                    .unwrap_or_default();
                if item.index as usize == rhs.len()
                    || !phase0
                        .grammar_data
                        .nullables
                        .are_nullable_symbols(&rhs[item.index as usize + 1..])
                {
                    continue;
                }
                for g_prime in gf_i_closure.get_relations_from(goto) {
                    if Symbol::Node(phase0.goto_data.symbol[g_prime]) == rhs[item.index as usize] {
                        set.insert(k as ItemIdx);
                    }
                }
            }
            follow_kernel_items.push(set);
        }
        GotoIdxVec(follow_kernel_items)
    }

    /// Computes [`Self::always_follows`].
    fn compute_always_follows(
        phase0: &Phase0,
        gf_i_closure: &GotoRelation,
    ) -> GotoIdxVec<Set<Lookahead>> {
        let mut always_follows = Vec::with_capacity(phase0.goto_data.from_state.len());
        for g in phase0.goto_data.get_all_goto_indices() {
            let mut set = Set::default();
            let g_node = phase0.goto_data.symbol[g];
            let g_from_state = phase0.goto_data.from_state[g];
            if phase0.lalr_table.starting_states.get(&g_node) == Some(&g_from_state) {
                set.insert(Lookahead::Eof);
            }
            for g_prime in gf_i_closure.get_relations_from(g) {
                set.extend(phase0.successor_follows[g_prime].iter().copied());
            }
            always_follows.push(set);
        }
        GotoIdxVec(always_follows)
    }

    fn compute_lookaheads(&mut self) {
        for state_index in self.lalr_table.states.all_indices() {
            let state = &self.lalr_table.states[state_index];
            for item_index in 0..state.items.len() as ItemIdx {
                Self::compute_lookaheads_item(
                    self.phase0.lalr_table,
                    &self.phase0.gotos_in_state,
                    &self.phase0.goto_follows,
                    &self.predecessors,
                    state_index,
                    item_index,
                );
            }
        }
    }

    /// Assume this item is a core item in position `k` in state `s`. Then its
    /// lookahead set is:
    /// - If `index = 0`, `lookaheads = { item_follows[item_index] }`
    /// - Else, union the lookahead sets of the predecessors of `s[k]`.
    pub(super) fn compute_lookaheads_item<'table, Kind>(
        table: &'table mut LRTable<Kind>,
        gotos_in_state: &[Vec<GotoIdx>],
        goto_follows: &GotoIdxVec<Set<Lookahead>>,
        predecessors: &[Predecessors<Kind>],
        state_index: StateIdx<Kind>,
        item_index: ItemIdx,
    ) -> &'table Set<Lookahead> {
        let item = &table.states[state_index].items[item_index as usize];
        if !item.lookaheads.is_empty() {
            return &table.states[state_index].items[item_index as usize].lookaheads;
        }
        if item.index == 0 {
            let g = Phase0::get_goto(table, gotos_in_state, state_index, item_index);
            let state = &mut table.states[state_index];
            let l = &mut state.items[item_index as usize].lookaheads;
            *l = goto_follows[g].clone();
            l
        } else {
            let mut lookaheads = Set::default();
            for (pred, pred_indices) in predecessors[state_index.get() as usize].clone() {
                let pred_index = pred_indices[item_index as usize];
                let pred_lookaheads = Self::compute_lookaheads_item(
                    table,
                    gotos_in_state,
                    goto_follows,
                    predecessors,
                    pred,
                    pred_index,
                );
                lookaheads.extend(pred_lookaheads.iter().copied());
            }
            let l =
                &mut table.states[state_index.get() as usize].items[item_index as usize].lookaheads;
            *l = lookaheads;
            l
        }
    }
}

impl<'a> Deref for Phase1<'a> {
    type Target = Phase0<'a>;

    fn deref(&self) -> &Self::Target {
        &self.phase0
    }
}

impl<'a> DerefMut for Phase1<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.phase0
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        grammar_data::GrammarData,
        ielr::tests::*,
        input::Symbol::{Node as N, Token as T},
        lr::LRTable,
        test_common::collect,
    };

    #[test]
    fn follow_kernel_items() {
        // boilerplate
        let grammar = grammar_2();
        let grammar_data = GrammarData::new(&grammar).unwrap();
        let mut lalr_table = LRTable::new_lr0(&grammar_data, collect(&[START])).unwrap();

        // Note that we don't represent states without non-core items.
        get_gotos! {in &lalr_table;
                        => S0 {START: g0_start};
            S0 => T(T1) => S1 {N1: g1_n1, N3: g1_n3, N7: g1_n7, N8: g1_n8};
            S1 => N(N1) => S2 {N2: g2_n2};
            S2 => N(N2) => S3 {N6: g3_n6, N7: g3_n7, N8: g3_n8};
            S1 => N(N8) => S7 {N3: g7_n3, N7: g7_n7, N8: g7_n8};
            S2 => T(T2) => S9 {N4: g9_n4, N6: g9_n6, N7: g9_n7, N8: g9_n8};
            S9 => N(N4) => S10 {N5: g10_n5};
        }
        let phase0 = crate::ielr::Phase0::new(&grammar_data, &mut lalr_table);
        let gf_i_closure = phase0.gf_i.closure();

        let follow_kernel_items = Phase1::compute_follow_kernel_items(&phase0, &gf_i_closure);

        assert_eq!(follow_kernel_items[g1_n1], collect(&[]));
        assert_eq!(follow_kernel_items[g1_n3], collect(&[]));
        assert_eq!(follow_kernel_items[g1_n7], collect(&[]));
        assert_eq!(follow_kernel_items[g1_n8], collect(&[]));
        assert_eq!(follow_kernel_items[g2_n2], collect(&[]));
        assert_eq!(follow_kernel_items[g3_n6], collect(&[0]));
        assert_eq!(follow_kernel_items[g3_n7], collect(&[0]));
        assert_eq!(follow_kernel_items[g3_n8], collect(&[0]));
        assert_eq!(follow_kernel_items[g7_n3], collect(&[0]));
        assert_eq!(follow_kernel_items[g7_n7], collect(&[0]));
        assert_eq!(follow_kernel_items[g7_n8], collect(&[0]));
        assert_eq!(follow_kernel_items[g9_n4], collect(&[0]));
        assert_eq!(follow_kernel_items[g9_n6], collect(&[1]));
        assert_eq!(follow_kernel_items[g9_n7], collect(&[1]));
        assert_eq!(follow_kernel_items[g9_n8], collect(&[1]));
        assert_eq!(follow_kernel_items[g10_n5], collect(&[0]));
    }

    #[test]
    fn always_follows() {
        // boilerplate
        let grammar = grammar_2();
        let grammar_data = GrammarData::new(&grammar).unwrap();
        let mut lalr_table = LRTable::new_lr0(&grammar_data, collect(&[START])).unwrap();

        get_gotos! {in &lalr_table;
                        => S0 {START: g0_start};
            S0 => T(T1) => S1 {N1: g1_n1, N3: g1_n3, N7: g1_n7, N8: g1_n8};
            S1 => N(N1) => S2 {N2: g2_n2};
            S2 => N(N2) => S3 {N6: g3_n6, N7: g3_n7, N8: g3_n8};
            S1 => N(N8) => S7 {N3: g7_n3, N7: g7_n7, N8: g7_n8};
            S2 => T(T2) => S9 {N4: g9_n4, N6: g9_n6, N7: g9_n7, N8: g9_n8};
            S9 => N(N4) => S10 {N5: g10_n5};
        }
        let phase0 = crate::ielr::Phase0::new(&grammar_data, &mut lalr_table);
        let gf_i_closure = phase0.gf_i.closure();

        let always_follows = Phase1::compute_always_follows(&phase0, &gf_i_closure);

        // check always_follows
        use crate::output::Lookahead::Token as LT;
        assert_eq!(always_follows[g1_n1], collect(&[LT(T2)]));
        assert_eq!(always_follows[g1_n3], collect(&[LT(T2)]));
        assert_eq!(always_follows[g1_n7], collect(&[LT(T2)]));
        assert_eq!(always_follows[g1_n8], collect(&[LT(T1), LT(T2), LT(T3)]));
        assert_eq!(always_follows[g2_n2], collect(&[LT(T1), LT(T3)]));
        assert_eq!(always_follows[g3_n6], collect(&[]));
        assert_eq!(always_follows[g3_n7], collect(&[]));
        assert_eq!(always_follows[g3_n8], collect(&[LT(T1), LT(T3)]));
        assert_eq!(always_follows[g7_n3], collect(&[]));
        assert_eq!(always_follows[g7_n7], collect(&[]));
        assert_eq!(always_follows[g7_n8], collect(&[LT(T1), LT(T3)]));
        assert_eq!(always_follows[g9_n4], collect(&[LT(T5)]));
        assert_eq!(always_follows[g9_n6], collect(&[]));
        assert_eq!(always_follows[g9_n7], collect(&[]));
        assert_eq!(always_follows[g9_n8], collect(&[LT(T1), LT(T3)]));
        assert_eq!(always_follows[g10_n5], collect(&[]));
    }
}
