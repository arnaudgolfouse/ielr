/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

//! Phase 2: Compute annotations
//!
//! This phase computes the kind of contributions states make to each conflict.

use super::Phase1;
use crate::{
    grammar_data::{ContributionIndex, GrammarData},
    indices::StateIdxRaw,
    input::Symbol,
    lr::{LRState, LRTable, StateList, LALR},
    output::{error::ConflictContributions, Lookahead},
    structures::{BitSet, Map, Set, Visit},
    GotoIdx, ItemIdx, StateIdx,
};
use std::{
    hash::Hash,
    ops::{Deref, DerefMut, Index},
    rc::Rc,
};

/// A conflict between multiple actions in a state.
#[derive(Clone, Debug)]
pub(crate) struct ConflictRoot {
    /// State in which the conflict occurs.
    pub(crate) s: StateIdx<LALR>,
    /// Lookahead on which the conflict occurs.
    pub(crate) lookahead: Lookahead,
    /// Contributions to the conflict.
    pub(crate) contributions: ConflictContributions,
}

/// Describes how the conflict's contributions are influenced by the current state's
/// items.
#[derive(Clone, Debug, PartialEq, Hash)]
pub(crate) struct PossibleContributions {
    /// The conflict contains a shift: this will always contribute.
    pub(crate) shift: bool,
    /// For the `i`-th reduce in the conflict:
    ///
    /// - If `reduces[i]` is `None`, the state always contribute to the reduce
    ///   contribution, no matter how it's split.
    /// - If this is `Some(bitset)`, then `bitset` is a vector of boolean over the
    ///   current state's items.
    ///   - If the boolean is `false`, the item never contribute to the reduce
    ///     contribution, no matter how we split the state.
    ///   - If the boolean is `true`, then the item of one (or more) of the states
    ///     split from the current state will bring a contribution to the reduce
    ///     contribution.
    // NOTE: since those come from ConflictRoot::contributions::reduce, the length of
    // this Vec is bounded by both `ProdIdxRaw` and `ItemIdx`
    pub(crate) reduces: Vec<Option<BitSet<ItemIdx>>>,
}

impl PossibleContributions {
    /// Determine if the contributions are _split-stable_.
    ///
    /// That is, if the state will never be split. In this case, we can skip this
    /// annotation.
    pub(crate) fn is_split_stable(&self) -> bool {
        let mut encountered_contribution = self.shift;
        for reduce_contribution in &self.reduces {
            if reduce_contribution
                .as_ref()
                .map(|set| !set.is_empty())
                .unwrap_or(true)
            {
                if encountered_contribution {
                    return false;
                }
                encountered_contribution = true;
            }
        }
        true
    }

    /// `true` if the contributions are all `None` or `Some(empty bitset)`.
    ///
    /// This is not exactly the same as [`Self::is_split_stable`], because the attached
    /// annotation should still exists. But, there is not use propagating it.
    pub(crate) fn only_always_or_never(&self) -> bool {
        for bitset in self.reduces.iter().flatten() {
            if !bitset.is_empty() {
                return false;
            }
        }
        true
    }
}

impl Index<ContributionIndex> for PossibleContributions {
    type Output = Option<BitSet<ItemIdx>>;

    fn index(&self, index: ContributionIndex) -> &Self::Output {
        match index {
            ContributionIndex::Shift => &None,
            ContributionIndex::Reduce(index) => &self.reduces[index.get() as usize - 1],
        }
    }
}

/// An annotation on a state, relating to a conflict.
///
/// This contains:
/// - A reference to the conflict.
/// - A list of _possible_ contributions.
#[derive(Clone, Debug)]
pub(crate) struct Annotation {
    /// conflict that this annotation refers to.
    pub(crate) conflict: Rc<ConflictRoot>,
    /// Describes how the conflict's contributions are influenced by the current state's
    /// items.
    pub(crate) possible_contributions: PossibleContributions,
}

impl PartialEq for Annotation {
    fn eq(&self, other: &Self) -> bool {
        Rc::as_ptr(&self.conflict) == Rc::as_ptr(&other.conflict)
            && self.possible_contributions == other.possible_contributions
    }
}
impl Eq for Annotation {}

impl Hash for Annotation {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        Rc::as_ptr(&self.conflict).hash(state);
        self.possible_contributions.hash(state);
    }
}

pub(crate) struct Phase2<'a> {
    pub(crate) phase1: Phase1<'a>,
    /// For each state, holds the list of conflicts.
    ///
    /// There is at most one conflict per state per lookahead.
    pub(crate) conflict_lists: Vec<Vec<Rc<ConflictRoot>>>,
    /// For each state, holds the conflict annotations.
    pub(crate) annotation_lists: Vec<Vec<Annotation>>,
}

impl<'a> Phase2<'a> {
    /// Compute the phase 0 of the IELR algorithm.
    ///
    /// We detect all possible conflict for phase 3 here.
    ///
    /// # Errors
    /// We can detect right now if a conflict will be unsolvable for phase 3, and
    /// report it.
    pub(crate) fn new(phase1: Phase1<'a>) -> Self {
        let conflict_lists = Self::compute_conflict_lists(&phase1);
        let annotation_lists = Self::compute_annotation_lists(&phase1, &conflict_lists);
        Self {
            phase1,
            conflict_lists,
            annotation_lists,
        }
    }

    /// Fill reductions in the table, and returns it, together with the accumulated
    /// statistics.
    ///
    /// # Warning
    /// Conflicts must have been resolved prior to this: else, this risk erasing some
    /// 'shift' actions.
    pub(crate) fn get_table(mut self) -> crate::output::Table {
        let mut table = LRTable {
            states: StateList::new(),
            starting_states: Map::default(),
        };
        std::mem::swap(self.lalr_table, &mut table);
        let mut table: crate::output::Table = table.into();
        table.finish(self.grammar_data);

        table
    }

    /// For each state, compute all possible conflicts.
    fn compute_conflict_lists(phase1: &Phase1) -> Vec<Vec<Rc<ConflictRoot>>> {
        let mut conflicts = Vec::new();
        for (s, state) in phase1
            .lalr_table
            .states
            .all_indices()
            .zip(phase1.lalr_table.states.iter())
        {
            let state_conflicts = Self::compute_conflicts_for_state(phase1.grammar_data, s, state);
            conflicts.push(state_conflicts);
        }
        conflicts
    }

    /// Compute conflicts found in a single state.
    fn compute_conflicts_for_state(
        grammar_data: &GrammarData,
        s: StateIdx<LALR>,
        state: &LRState<LALR>,
    ) -> Vec<Rc<ConflictRoot>> {
        let mut state_conflicts = Vec::new();
        let mut reduces: Map<_, Set<Lookahead>> = Map::default();
        let mut shifts = Set::default();
        let mut all_lookaheads = Set::default();
        for item in state.all_items() {
            let rhs = grammar_data.get_rhs(item.prod_idx).unwrap_or_default();
            match rhs.get(item.index as usize) {
                Some(&Symbol::Token(t)) => {
                    shifts.insert(t);
                    all_lookaheads.insert(Lookahead::Token(t));
                }
                None => {
                    // Precedence annotations may generate non-conflicting reduces (e.g. E → E + E∙ [_, _] and E → E + E∙ [_, 2]).
                    reduces
                        .entry(item.prod_idx)
                        .or_default()
                        .extend(&item.lookaheads);
                    all_lookaheads.extend(item.lookaheads.iter().copied());
                }
                _ => {}
            }
        }
        for lookahead in all_lookaheads {
            let mut contributions = ConflictContributions::default();
            for (production, lookaheads) in &reduces {
                if lookaheads.contains(&lookahead) {
                    contributions.reduces.push(*production);
                }
            }
            if let Lookahead::Token(token) = lookahead {
                if shifts.contains(&token) {
                    contributions.has_shift = true;
                }
            }

            if contributions.len() > 1 {
                state_conflicts.push(Rc::new(ConflictRoot {
                    s,
                    lookahead,
                    contributions,
                }));
            }
        }
        state_conflicts
    }

    /// For each conflict, trace how states should be splitted.
    fn compute_annotation_lists(
        phase1: &Phase1,
        conflicts: &[Vec<Rc<ConflictRoot>>],
    ) -> Vec<Vec<Annotation>> {
        let mut annotation_lists = vec![Vec::new(); phase1.lalr_table.states.len().get() as usize];
        let mut to_visit = Visit::<_, crate::structures::Hasher>::new();
        for (s, conflicts) in conflicts.iter().enumerate() {
            let s = StateIdx::new(s as StateIdxRaw);
            for conflict in conflicts.iter().cloned() {
                let annotation = Self::annotate_manifestation(phase1, conflict);
                to_visit.insert((s, annotation));
            }
        }
        while let Some((s, annotation)) = to_visit.pop() {
            if !annotation.possible_contributions.only_always_or_never() {
                for (&predecessor, predecessor_indices) in &phase1.predecessors[s.get() as usize] {
                    let annotation = Self::annotate_predecessor(
                        phase1,
                        (predecessor, predecessor_indices),
                        s,
                        &annotation,
                    );
                    if !annotation.possible_contributions.is_split_stable() {
                        to_visit.insert((predecessor, annotation));
                    }
                }
            }
        }

        for (state, annotation) in to_visit {
            annotation_lists[state.get() as usize].push(annotation);
        }

        annotation_lists
    }

    /// Generate the [`Annotation`] corresponding to `conflict`, in state
    /// [`conflict.s`](`ConflictRoot::s`).
    fn annotate_manifestation(phase1: &Phase1, conflict: Rc<ConflictRoot>) -> Annotation {
        let s = conflict.s;
        let mut possible_contributions = PossibleContributions {
            shift: conflict.contributions.has_shift,
            reduces: Vec::new(),
        };

        for &production in &conflict.contributions.reduces {
            possible_contributions.reduces.push({
                let rhs = phase1.grammar_data.get_rhs(production).unwrap_or_default();

                if !rhs.is_empty() {
                    let mut set = BitSet::new();
                    let state = &phase1.lalr_table.states[s];
                    for (i, item) in state.core_items().iter().enumerate() {
                        if item.prod_idx == production && item.index as usize == rhs.len() {
                            set.insert(i as ItemIdx);
                            break;
                        }
                    }
                    Some(set)
                } else {
                    let mut result = None;
                    for g in phase1.get_gotos_in_state(s) {
                        if phase1.goto_data.productions[g].contains(&production) {
                            result = Some(Self::compute_lhs_contributions(
                                phase1,
                                s,
                                g,
                                conflict.lookahead,
                            ));
                            break;
                        }
                    }
                    result.unwrap()
                }
            });
        }
        Annotation {
            conflict,
            possible_contributions,
        }
    }

    /// Compute how other core items' lookaheads in `s` might contribute `lookahead`
    /// to the non-core item `i`.
    ///
    /// # Returns
    /// - `None` if an item always contribute `lookahead`, no matter how `s` is
    ///   split.
    /// - `Some(bitset)` else, with `bitset.contains(i)` if the `i`-th item gives
    ///   its lookaheads to `node → ∙α`, and its LALR counterpart's lookaheads contain
    ///   `lookahead`.
    fn compute_lhs_contributions(
        phase1: &Phase1,
        s: StateIdx<LALR>,
        g: GotoIdx,
        lookahead: Lookahead,
    ) -> Option<BitSet<ItemIdx>> {
        if phase1.always_follows[g].contains(&lookahead) {
            // first case
            return None;
        }
        let mut set = BitSet::new();
        let follow_kernel_items_i = &phase1.follow_kernel_items[g];
        for core_item_index in follow_kernel_items_i.iter() {
            let item_lookahead_set =
                &phase1.lalr_table.states[s].core_items()[core_item_index as usize].lookaheads;
            if item_lookahead_set.contains(&lookahead) {
                set.insert(core_item_index);
            }
        }
        Some(set)
    }

    /// Propagate `annotation` to a predecessor of `s`.
    ///
    /// # Parameters
    /// - `predecessor`: a predecessor of `s`. This will be the state that receives the
    ///   annotation.
    /// - `s`: a state that already received an annotation.
    /// - `annotation`: an annotation on `s`.
    ///
    /// # Return
    /// Let us designate by `t` the lookahead on which the conflict of `annotation`
    /// occurs.
    ///
    /// For a given contribution index `ci`, the corresponding
    /// `possible_contributions[ci]` in the returned annotation will be:
    /// 1. `None` if
    ///    1. `annotation.possible_contributions[ci]` is `None`, or
    ///    2.
    ///       - `annotation.possible_contributions[ci].contains(k)`, and
    ///       - The `k`-th item of `s` is `l' → x∙β'`, and `g` is the
    ///               [`GotoIdx`] in `predecessor` with production `l' → xβ'`, and
    ///       - `compute_lhs_contributions(predecessor, s, t)` is `None`.
    /// 2. Else, for `j` in `0 .. predecessor.core_items.len`,
    ///    `possible_contributions[ci]` contains `j` if
    ///    - `annotation.possible_contributions[ci].contains(k)`, and
    ///    - If the `j`-th item of `predecessor` is `l → α∙β` and the `k`-th item of
    ///      `s` is `l' → α'∙β'`, we have either
    ///      1. `l' → α'∙β'` is `l → α∙β` with the dot moved to the right from 1, and
    ///         `t` is in the lookaheads of `l → α∙β`.
    ///      2. `|α| = 1`, there exists `g` a [`GotoIdx`] in `predecessor` such that
    ///         `∃P∈goto_data.production[g], P == l → αβ`, and
    ///         `compute_lhs_contributions(predecessor, l', t)` contains `j`.
    fn annotate_predecessor(
        phase1: &Phase1,
        (predecessor, _): (StateIdx<LALR>, &[ItemIdx]),
        s: StateIdx<LALR>,
        annotation: &Annotation,
    ) -> Annotation {
        let predecessor_state = &phase1.lalr_table.states[predecessor];
        let state = &phase1.lalr_table.states[s];
        let mut items_contributions = PossibleContributions {
            shift: annotation.possible_contributions.shift,
            reduces: Vec::new(),
        };
        'contribution: for reduce_contribution in &annotation.possible_contributions.reduces {
            let contribution = match reduce_contribution {
                Some(c) => c,
                None => {
                    items_contributions.reduces.push(None);
                    continue;
                }
            };
            for (j, item) in state.core_items().iter().enumerate() {
                if !contribution.contains(j as ItemIdx) || item.index != 1 {
                    continue;
                }
                let g = {
                    let mut res = None;
                    for g in phase1.get_gotos_in_state(predecessor) {
                        if phase1.goto_data.productions[g].contains(&item.prod_idx) {
                            res = Some(g);
                            break;
                        }
                    }
                    res.unwrap()
                };
                if Self::compute_lhs_contributions(
                    phase1,
                    predecessor,
                    g,
                    annotation.conflict.lookahead,
                )
                .is_none()
                {
                    items_contributions.reduces.push(None);
                    continue 'contribution;
                }
            }
            let mut new_bitset = BitSet::new();
            'j: for (j, predecessor_item) in predecessor_state.core_items().iter().enumerate() {
                for (k, item) in state.core_items().iter().enumerate() {
                    if !contribution.contains(k as ItemIdx) {
                        continue;
                    }
                    if predecessor_item.prod_idx == item.prod_idx
                        && predecessor_item.index + 1 == item.index
                        && predecessor_item
                            .lookaheads
                            .contains(&annotation.conflict.lookahead)
                    {
                        new_bitset.insert(j as ItemIdx);
                        continue 'j;
                    }
                    if item.index == 1 {
                        let g = {
                            let mut res = None;
                            for g in phase1.get_gotos_in_state(predecessor) {
                                if phase1.goto_data.productions[g].contains(&item.prod_idx) {
                                    res = Some(g);
                                    break;
                                }
                            }
                            res.unwrap()
                        };
                        if Self::compute_lhs_contributions(
                            phase1,
                            predecessor,
                            g,
                            annotation.conflict.lookahead,
                        )
                        .map(|bitset| bitset.contains(j as ItemIdx))
                        .unwrap_or(false)
                        {
                            new_bitset.insert(j as ItemIdx);
                            continue 'j;
                        }
                    }
                }
            }
            items_contributions.reduces.push(Some(new_bitset));
        }

        Annotation {
            conflict: Rc::clone(&annotation.conflict),
            possible_contributions: items_contributions,
        }
    }
}

impl<'a> Deref for Phase2<'a> {
    type Target = Phase1<'a>;

    fn deref(&self) -> &Self::Target {
        &self.phase1
    }
}

impl<'a> DerefMut for Phase2<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.phase1
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        grammar_data::GrammarData,
        ielr::tests::*,
        input::Symbol::{Node as N, Token as T},
        lr::LRTable,
        test_common::collect,
    };

    #[test]
    fn annotations_split_stable() {
        let mut possible_contributions = PossibleContributions {
            shift: false,
            reduces: Vec::new(),
        };
        assert!(possible_contributions.is_split_stable());

        possible_contributions = PossibleContributions {
            shift: false,
            reduces: vec![Some(BitSet::new())],
        };
        assert!(possible_contributions.is_split_stable());

        possible_contributions = PossibleContributions {
            shift: false,
            reduces: vec![Some(BitSet::new()), None, Some(BitSet::new())],
        };
        assert!(possible_contributions.is_split_stable());

        possible_contributions = PossibleContributions {
            shift: true,
            reduces: vec![Some(BitSet::new())],
        };
        assert!(possible_contributions.is_split_stable());

        let mut non_empty_bitset1 = BitSet::new();
        let mut non_empty_bitset2 = BitSet::new();
        non_empty_bitset1.insert(0);
        non_empty_bitset2.insert(1000);
        possible_contributions = PossibleContributions {
            shift: false,
            reduces: vec![
                Some(BitSet::new()),
                Some(non_empty_bitset1.clone()),
                Some(BitSet::new()),
            ],
        };
        assert!(possible_contributions.is_split_stable());

        possible_contributions = PossibleContributions {
            shift: false,
            reduces: vec![
                Some(BitSet::new()),
                Some(non_empty_bitset1.clone()),
                Some(non_empty_bitset2),
            ],
        };
        assert!(!possible_contributions.is_split_stable());

        possible_contributions = PossibleContributions {
            shift: true,
            reduces: vec![Some(BitSet::new()), Some(non_empty_bitset1)],
        };
        assert!(!possible_contributions.is_split_stable());
    }

    fn state_from_path<'a>(
        table: &'a LRTable<LALR>,
        path: &[Symbol],
    ) -> (StateIdx<LALR>, &'a LRState<LALR>) {
        let mut state_index = table.starting_states[&START];
        let mut state = &table.states[state_index];
        for symbol in path {
            state_index = state.transitions[symbol];
            state = &table.states[state_index];
        }
        (state_index, state)
    }

    #[test]
    fn compute_conflicts() {
        // boilerplate
        let grammar = grammar_2();
        let grammar_data = GrammarData::new(&grammar).unwrap();
        let mut lalr_table = LRTable::new_lr0(&grammar_data, collect(&[START])).unwrap();
        let phase0 = crate::ielr::Phase0::new(&grammar_data, &mut lalr_table);
        let phase1 = Phase1::new(phase0);

        // two states contain conflicts.
        let all_conflicts = Phase2::compute_conflict_lists(&phase1);
        assert_eq!(all_conflicts.iter().filter(|c| !c.is_empty()).count(), 2);

        // Check those two states.
        let (state_7_index, state_7) =
            state_from_path(&lalr_table, &[Symbol::Token(T1), Symbol::Node(N8)]);
        let (state_16_index, state_16) =
            state_from_path(&lalr_table, &[T(T1), N(N1), T(T2), T(T1)]);
        let state_7_conflicts =
            Phase2::compute_conflicts_for_state(&grammar_data, state_7_index, state_7);
        assert_eq!(state_7_conflicts.len(), 2);
        for conflict in state_7_conflicts {
            assert!(conflict.contributions.has_shift);
            assert_eq!(conflict.contributions.reduces.len(), 1);
        }
        let state_16_conflicts =
            Phase2::compute_conflicts_for_state(&grammar_data, state_16_index, state_16);
        assert_eq!(state_16_conflicts.len(), 2);
        assert_eq!(
            state_16_conflicts
                .iter()
                .map(|c| c.lookahead)
                .collect::<Set<_>>(),
            collect(&[Lookahead::Token(T1), Lookahead::Token(T3)])
        );
        for conflict in state_16_conflicts {
            assert!(!conflict.contributions.has_shift);
            assert_eq!(conflict.contributions.reduces.len(), 2);
        }
    }

    #[test]
    fn compute_lhs_contributions() {
        // boilerplate
        let grammar = grammar_2();
        let grammar_data = GrammarData::new(&grammar).unwrap();
        let mut lalr_table = LRTable::new_lr0(&grammar_data, collect(&[START])).unwrap();
        let phase0 = crate::ielr::Phase0::new(&grammar_data, &mut lalr_table);
        let phase1 = Phase1::new(phase0);

        // Note that we don't represent states without non-core items.
        get_gotos! {in phase1.lalr_table;
                        => S0 {START: g0_start};
            S0 => T(T1) => S1 {N1: g1_n1, N3: g1_n3, N7: g1_n7, N8: g1_n8};
            S1 => N(N1) => S2 {N2: g2_n2};
            S2 => N(N2) => S3 {N6: g3_n6, N7: g3_n7, N8: g3_n8};
            S1 => N(N8) => S7 {N3: g7_n3, N7: g7_n7, N8: g7_n8};
            S2 => T(T2) => S9 {N4: g9_n4, N6: g9_n6, N7: g9_n7, N8: g9_n8};
            S9 => N(N4) => S10 {N5: g10_n5};
        }

        let (state_7_index, _) =
            state_from_path(phase1.lalr_table, &[Symbol::Token(T1), Symbol::Node(N8)]);
        for (index, (g, lookahead, result)) in [
            (g7_n3, Lookahead::Eof, Some(collect(&[0]))),
            (g7_n3, Lookahead::Token(T1), Some(collect(&[0]))),
            (g7_n3, Lookahead::Token(T2), Some(collect(&[0]))),
            (g7_n3, Lookahead::Token(T3), Some(collect(&[0]))),
            (g7_n7, Lookahead::Token(T1), Some(collect(&[0]))),
            (g7_n7, Lookahead::Token(T3), Some(collect(&[0]))),
            (g7_n8, Lookahead::Token(T1), None),
            (g7_n8, Lookahead::Token(T2), Some(collect(&[0]))),
            (g7_n8, Lookahead::Token(T3), None),
        ]
        .into_iter()
        .enumerate()
        {
            assert_eq!(
                Phase2::compute_lhs_contributions(&phase1, state_7_index, g, lookahead),
                result,
                "failed at index {index}"
            );
        }

        let (state_9_index, _) = state_from_path(
            phase1.lalr_table,
            &[Symbol::Token(T1), Symbol::Node(N1), Symbol::Token(T2)],
        );

        for (index, (g, lookahead, result)) in [
            (g9_n4, Lookahead::Eof, Some(collect(&[]))),
            (g9_n4, Lookahead::Token(T1), Some(collect(&[0]))),
            (g9_n4, Lookahead::Token(T2), Some(collect(&[]))),
            (g9_n4, Lookahead::Token(T3), Some(collect(&[0]))),
            (g9_n4, Lookahead::Token(T5), None),
            (g9_n6, Lookahead::Token(T1), Some(collect(&[1]))),
            (g9_n6, Lookahead::Token(T2), Some(collect(&[]))),
            (g9_n6, Lookahead::Token(T3), Some(collect(&[1]))),
            (g9_n6, Lookahead::Token(T5), Some(collect(&[]))),
            (g9_n7, Lookahead::Token(T1), Some(collect(&[1]))),
            (g9_n7, Lookahead::Token(T2), Some(collect(&[]))),
            (g9_n7, Lookahead::Token(T3), Some(collect(&[1]))),
            (g9_n7, Lookahead::Token(T5), Some(collect(&[]))),
            (g9_n8, Lookahead::Token(T1), None),
            (g9_n8, Lookahead::Token(T2), Some(collect(&[]))),
            (g9_n8, Lookahead::Token(T3), None),
            (g9_n8, Lookahead::Token(T5), Some(collect(&[]))),
        ]
        .into_iter()
        .enumerate()
        {
            assert_eq!(
                Phase2::compute_lhs_contributions(&phase1, state_9_index, g, lookahead),
                result,
                "{index}-th test"
            );
        }
    }

    #[test]
    fn annotate_predecessor() {
        // === boilerplate ===
        let grammar = grammar_2();
        let grammar_data = GrammarData::new(&grammar).unwrap();
        let mut lalr_table = LRTable::new_lr0(&grammar_data, collect(&[START])).unwrap();
        let phase0 = crate::ielr::Phase0::new(&grammar_data, &mut lalr_table);
        let phase1 = Phase1::new(phase0);
        let predecessors = &phase1.predecessors;

        // === Conflict on state 16, on token T3 ===
        let (state_2_index, _) =
            state_from_path(phase1.lalr_table, &[Symbol::Token(T1), Symbol::Node(N1)]);
        let (state_9_index, _) = state_from_path(
            phase1.lalr_table,
            &[Symbol::Token(T1), Symbol::Node(N1), Symbol::Token(T2)],
        );
        let (state_16_index, state_16) = state_from_path(
            phase1.lalr_table,
            &[
                Symbol::Token(T1),
                Symbol::Node(N1),
                Symbol::Token(T2),
                Symbol::Token(T1),
            ],
        );
        let state_16_conflicts =
            Phase2::compute_conflicts_for_state(&grammar_data, state_16_index, state_16);
        let conflict_on_t3 = state_16_conflicts
            .into_iter()
            .find(|conflict| conflict.lookahead == Lookahead::Token(T3))
            .unwrap();
        let (n4_index, n8_index) = // Make comparison possible :p
            if conflict_on_t3.contributions.reduces.first().unwrap().lhs == N4 {
                (ContributionIndex::new_reduce(0), ContributionIndex::new_reduce(1))
            } else {
                (ContributionIndex::new_reduce(1), ContributionIndex::new_reduce(0))
            };
        let annotation_root = Phase2::annotate_manifestation(&phase1, conflict_on_t3);
        let annotation_1 = Phase2::annotate_predecessor(
            &phase1,
            (
                state_9_index,
                &predecessors[state_16_index.get() as usize][&state_9_index],
            ),
            state_16_index,
            &annotation_root,
        );
        // Case 2.2
        assert_eq!(
            annotation_1.possible_contributions[n4_index],
            Some(collect(&[0]))
        );
        // Case 1.2
        assert_eq!(annotation_1.possible_contributions[n8_index], None);

        let annotation_2 = Phase2::annotate_predecessor(
            &phase1,
            (
                state_2_index,
                &predecessors[state_9_index.get() as usize][&state_2_index],
            ),
            state_9_index,
            &annotation_1,
        );
        // Case 1.2
        assert_eq!(annotation_2.possible_contributions[n4_index], None);
        // Case 1.1
        assert_eq!(annotation_2.possible_contributions[n8_index], None);
    }

    #[test]
    fn annotate_predecessor_case_2_1() {
        // === boilerplate ===
        let grammar = grammar_3();
        let grammar_data = GrammarData::new(&grammar).unwrap();
        let mut lalr_table = LRTable::new_lr0(&grammar_data, collect(&[START])).unwrap();
        let phase0 = crate::ielr::Phase0::new(&grammar_data, &mut lalr_table);
        let phase1 = Phase1::new(phase0);
        let predecessors = &phase1.predecessors;

        // === Conflict on state 3, on token EOF ===
        let (state_1_index, _) = state_from_path(phase1.lalr_table, &[Symbol::Token(T1)]);
        let (state_2_index, _) =
            state_from_path(phase1.lalr_table, &[Symbol::Token(T1), Symbol::Token(T2)]);
        let (state_3_index, state_3) = state_from_path(
            phase1.lalr_table,
            &[Symbol::Token(T1), Symbol::Token(T2), Symbol::Token(T3)],
        );
        let state_3_conflicts =
            Phase2::compute_conflicts_for_state(&grammar_data, state_3_index, state_3);
        let conflict_on_eof = state_3_conflicts
            .into_iter()
            .find(|conflict| conflict.lookahead == Lookahead::Eof)
            .unwrap();
        let n1_index = // Make comparison possible :p
            if conflict_on_eof.contributions.reduces.first().unwrap().lhs == N1 {
                ContributionIndex::new_reduce(0)
            } else {
                ContributionIndex::new_reduce(1)
            };
        let annotation_root = Phase2::annotate_manifestation(&phase1, conflict_on_eof);
        let annotation_1 = Phase2::annotate_predecessor(
            &phase1,
            (
                state_2_index,
                &predecessors[state_3_index.get() as usize][&state_2_index],
            ),
            state_3_index,
            &annotation_root,
        );
        // Case 2.2
        assert_eq!(
            annotation_1.possible_contributions[n1_index],
            Some(collect(&[0]))
        );
        let annotation_2 = Phase2::annotate_predecessor(
            &phase1,
            (
                state_1_index,
                &predecessors[state_2_index.get() as usize][&state_1_index],
            ),
            state_2_index,
            &annotation_1,
        );
        // Case 2.1
        assert_eq!(
            annotation_2.possible_contributions[n1_index],
            Some(collect(&[0]))
        );
    }
}
