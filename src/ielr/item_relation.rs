/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

//! Defines the [`GotoRelation`] structure.

use crate::{structures::BitSet, GotoIdx};

/// A binary relation between [`GotoIdx`]s.
///
/// # WARNING
///
/// The methods of this type **only** make sense (aka don't panic) if the
/// [`GotoIdx`] you pass them are always _less_ than the `items_len` parameter of
/// [`new`](Self::new).
// TODO(perf): the `BitSet` here are often almost empty: using a 'sparse' bitset might be beneficial cache-wise.
#[derive(Clone, Debug, Default)]
pub(crate) struct GotoRelation {
    /// Maps `from` to all the `to`, such that `(from, to)` is in the relation.
    relation: Vec<BitSet<GotoIdx>>,
    /// Maps `to` to all the `from`, such that `(from, to)` is in the relation.
    reverse_relation: Vec<BitSet<GotoIdx>>,
}

impl GotoRelation {
    /// Creates a new empty relation.
    pub(crate) fn new(items_len: GotoIdx) -> Self {
        Self {
            relation: vec![BitSet::with_zeroed(items_len as usize); items_len as usize],
            reverse_relation: vec![BitSet::with_zeroed(items_len as usize); items_len as usize],
        }
    }

    /// Returns `true` if the relation `(from, to)` was not already present.
    pub(crate) fn add(&mut self, from: GotoIdx, to: GotoIdx) -> bool {
        if !self.relation[from as usize].insert(to) {
            return false;
        }
        self.reverse_relation[to as usize].insert(from);
        true
    }

    #[cfg(test)]
    pub(crate) fn contains(&self, from: GotoIdx, to: GotoIdx) -> bool {
        match self.relation.get(from as usize) {
            Some(bitset) => bitset.contains(to),
            None => false,
        }
    }

    /// Get all items `to` such that `(from, to)` is in the relation.
    pub(crate) fn get_relations_from(&self, from: GotoIdx) -> impl Iterator<Item = GotoIdx> + '_ {
        self.relation
            .get(from as usize)
            .map(|set| set.iter().map(|n| n as GotoIdx))
            .into_iter()
            .flatten()
    }

    /// Get all items `from` such that `(from, to)` is in the relation.
    #[cfg(test)]
    pub(crate) fn get_relations_to(&self, to: GotoIdx) -> impl Iterator<Item = GotoIdx> + '_ {
        self.reverse_relation
            .get(to as usize)
            .map(|set| set.iter().map(|n| n as GotoIdx))
            .into_iter()
            .flatten()
    }

    /// Get all the relations `(from, to)`.
    pub(crate) fn get_all_relations(&self) -> impl Iterator<Item = (GotoIdx, GotoIdx)> + '_ {
        self.relation.iter().enumerate().flat_map(|(from, to_set)| {
            to_set
                .iter()
                .map(move |to| (from as GotoIdx, to as GotoIdx))
        })
    }

    /// Compute the transitive closure of the given relation.
    ///
    /// # Note
    /// All [`GotoIdx`] will be related to itself in the closure (provided the
    /// length supplied in [`Self::new`] was correct).
    pub(crate) fn closure(&self) -> Self {
        let length = self.relation.len();
        let mut current_goto_relations: BitSet<GotoIdx> = BitSet::new();
        let mut current_xor_relation: BitSet<GotoIdx>;
        let mut closure_relation = self.relation.clone();
        let mut xor_relation = self.relation.clone();
        for i in 0..self.relation.len() {
            std::mem::swap(&mut closure_relation[i], &mut current_goto_relations);
            let mut modified = true;
            while modified {
                current_xor_relation = BitSet::with_zeroed(length);
                std::mem::swap(&mut xor_relation[i], &mut current_xor_relation);
                for j in current_xor_relation.iter() {
                    let j = j as usize;
                    if i == j {
                        continue;
                    }
                    for ((x, y), xor) in current_goto_relations
                        .as_mut_slice()
                        .iter_mut()
                        .zip(closure_relation[j].as_slice())
                        .zip(xor_relation[i].as_mut_slice())
                    {
                        let old_x = *x;
                        *x = old_x | y;
                        *xor |= *x ^ old_x;
                    }
                }
                modified = !xor_relation[i].is_empty();
            }
            std::mem::swap(&mut closure_relation[i], &mut current_goto_relations);
        }
        let mut closure_reverse_relation = self.reverse_relation.clone();
        for (i, relation) in closure_relation.iter().enumerate() {
            for j in relation.iter() {
                closure_reverse_relation[j as usize].insert(i as GotoIdx);
            }
        }
        for i in 0..self.relation.len() {
            closure_relation[i].insert(i as GotoIdx);
            closure_reverse_relation[i].insert(i as GotoIdx);
        }
        Self {
            relation: closure_relation,
            reverse_relation: closure_reverse_relation,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::structures::Set;

    fn make_item_relation(slice: &[(GotoIdx, GotoIdx)]) -> GotoRelation {
        let mut max_item = 0;
        for (g1, g2) in slice {
            if *g1 > max_item {
                max_item = *g1;
            }
            if *g2 > max_item {
                max_item = *g2;
            }
        }
        let mut relation = GotoRelation::new(max_item + 1);
        for (from, to) in slice {
            relation.add(*from, *to);
        }
        relation
    }

    #[test]
    fn get_relations_from() {
        let relation: GotoRelation =
            make_item_relation(&[(0, 0), (0, 1), (1, 2), (1, 4), (2, 4), (3, 5)]);
        //          0           1           2        3        4
        let sets = [vec![0, 1], vec![2, 4], vec![4], vec![5], vec![]];
        for (i, set) in sets.into_iter().enumerate() {
            let i = i as GotoIdx;
            let set = set.into_iter().collect::<Set<_>>();
            assert_eq!(relation.get_relations_from(i).collect::<Set<_>>(), set);
        }
    }

    #[test]
    fn get_relations_to() {
        let relation: GotoRelation =
            make_item_relation(&[(0, 0), (0, 1), (1, 2), (1, 4), (3, 4), (3, 2)]);
        //          0        1        2           3       4
        let sets = [vec![0], vec![0], vec![1, 3], vec![], vec![1, 3]];
        for (i, set) in sets.into_iter().enumerate() {
            let i = i as GotoIdx;
            let set = set.into_iter().collect::<Set<_>>();
            assert_eq!(relation.get_relations_to(i).collect::<Set<_>>(), set);
        }
    }

    #[test]
    fn closure() {
        let relation: GotoRelation =
            make_item_relation(&[(0, 0), (0, 1), (1, 2), (1, 3), (2, 4), (5, 4)]);

        let closure = relation.closure();
        assert_eq!(
            closure.get_all_relations().collect::<Set<_>>(),
            [
                (0, 0),
                (0, 1),
                (0, 2),
                (0, 3),
                (0, 4),
                (1, 1),
                (1, 2),
                (1, 3),
                (1, 4),
                (2, 2),
                (2, 4),
                (3, 3),
                (4, 4),
                (5, 4),
                (5, 5),
            ]
            .into_iter()
            .collect::<Set<_>>()
        );
    }
}
