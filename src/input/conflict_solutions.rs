/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use super::{Node, ProdIdx};
use crate::output::Lookahead;

/// A manual solution to a conflict in the grammar.
///
/// # Example
/// ```
/// use ielr::{
///     input::{ConflictSolution, ConflictingAction, Grammar, Node, Symbol, Token},
///     output::Lookahead,
/// };
/// let expr: Node = Node(0);
/// let plus: Token = Token::new(1).unwrap();
/// let int: Token = Token::new(2).unwrap();
/// let mut grammar = Grammar::new();
/// let production = grammar
///     .add_production(
///         expr,
///         vec![Symbol::Node(expr), Symbol::Token(plus), Symbol::Node(expr)],
///     )
///     .unwrap();
/// grammar
///     .add_production(expr, vec![Symbol::Token(int)])
///     .unwrap();
/// grammar.add_conflict_solution(ConflictSolution {
///     prefer: ConflictingAction::Reduce(production),
///     over: ConflictingAction::Shift(Lookahead::Token(plus)),
/// });
/// ```
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ConflictSolution {
    /// The action that will be preferred over `over`.
    ///
    /// This does not mean that this action wil always be kept: there might be other
    /// [`ConflictingAction`] that remove it.
    pub prefer: ConflictingAction,
    /// The action that will be removed in favor of `prefer`.
    pub over: ConflictingAction,
}

/// One of the possible actions in [`ConflictSolution`].
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ConflictingAction {
    /// Matches any 'shift' action.
    AnyShift,
    /// Matches a 'shift' action on the given lookahead.
    Shift(Lookahead),
    /// Matches any 'reduce' action.
    AnyReduce,
    /// Matches a 'reduce' action on the given production.
    Reduce(ProdIdx),
    /// Matches a 'reduce' action on any production that has the given node as left-hand
    /// side.
    ReduceNode(Node),
}
