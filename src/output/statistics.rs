/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use crate::Algorithm;

/// Statistics gathered while running the IELR algorithm.
#[derive(Clone, Debug)]
pub struct Statistics {
    /// Number of states in the LALR state machine.
    pub lalr_states_count: usize,
    /// Number of states added while using LR(1).
    pub lr1_states_count: usize,
    /// The indices in [`Grammar::conflict_solutions`] of the solutions used during the
    /// algorithm.
    ///
    /// Users may be interested in solution **not** used, since they were useless during
    /// the algorithm.
    ///
    /// [`Grammar::conflict_solutions`]: crate::input::Grammar::get_conflict_solutions
    pub conflict_resolution_used: Vec<usize>,
    /// What level of the algorithm was used in case of success.
    pub algorithm_needed: Algorithm,
}
