/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use crate::input::Token;
use std::{fmt, hash};

/// Either a [`Token`], or `EOF`.
#[derive(Clone, Copy, Eq, PartialOrd, Ord)]
pub enum Lookahead {
    /// End-of-file.
    ///
    /// Sometimes, a rule will have this as lookahead: this means that this rule may
    /// appear at the very end of the text.
    ///
    /// For example, in Rust code:
    /// - A `let` binding _may not_ appear at the end of a file, and thus can never be
    ///   followed by `Eof`.
    /// - A function _may_ appear at the end of a file, and thus can be followed by
    ///   `Eof`.
    Eof,
    /// A normal token: see [`Token`].
    Token(Token),
}

impl Lookahead {
    const fn plain(self) -> crate::indices::PlainToken {
        match self {
            Self::Eof => 0,
            Self::Token(t) => t.get(),
        }
    }
}

impl PartialEq for Lookahead {
    fn eq(&self, other: &Self) -> bool {
        self.plain() == other.plain()
    }
}

impl hash::Hash for Lookahead {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        self.plain().hash(state);
    }
}

impl fmt::Debug for Lookahead {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Eof => write!(f, "Eof"),
            Self::Token(t) => write!(f, "Token({:?})", t.get()),
        }
    }
}
