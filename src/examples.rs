/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

//! Complex examples.

// This module is a trick to
// - Make the example appear in rustdoc
// - Run the examples with `cargo test`

pub mod grammar_with_conflicts {
    #![doc = "```"]
    #![doc = include_str!("../examples/grammar_with_conflicts.rs")]
    #![doc = "```"]
}

pub mod grammar_with_precedence {
    #![doc = "```"]
    #![doc = include_str!("../examples/grammar_with_precedence.rs")]
    #![doc = "```"]
}
