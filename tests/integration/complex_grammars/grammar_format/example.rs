/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use ielr::input::Token;
use std::collections::HashMap;

pub(super) fn parse_example(
    line: &str,
    tokens: &HashMap<&str, Token>,
) -> Result<Vec<Token>, String> {
    let mut result = Vec::new();
    let line = line.trim();
    let line = match line.strip_prefix('(') {
        Some(line) => match line.strip_suffix(')') {
            Some(line) => line,
            None => return Err(String::from("missing closing ')' after @example")),
        },
        None => return Err(String::from("expected '(' after @example")),
    };
    for word in line.split_whitespace() {
        let token = match tokens.get(word) {
            Some(t) => *t,
            None => return Err(format!("unknown token: '{word}'")),
        };
        result.push(token);
    }
    Ok(result)
}

pub(super) fn parse_example_fail(
    line: &str,
    tokens: &HashMap<&str, Token>,
) -> Result<Vec<Token>, String> {
    let mut result = Vec::new();
    let line = line.trim();
    let line = match line.strip_prefix('(') {
        Some(line) => match line.strip_suffix(')') {
            Some(line) => line,
            None => return Err(String::from("missing closing ')' after @example")),
        },
        None => return Err(String::from("expected '(' after @example")),
    };
    for word in line.split_whitespace() {
        let token = match tokens.get(word) {
            Some(t) => *t,
            None => return Err(format!("unknown token: '{word}'")),
        };
        result.push(token);
    }
    Ok(result)
}
