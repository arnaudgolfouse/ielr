/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use std::{
    fs, io,
    path::{Path, PathBuf},
};

const LICENSE_TEXT: &str = r"/*
 * Copyright 2022-2024 Arnaud Golfouse
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */";

const SOURCE_DIRS: &[&str] = &["src", "tests", "examples"];

fn get_all_rust_file_paths() -> Result<Vec<PathBuf>, io::Error> {
    fn get_all_rust_file_paths_inner(
        path: PathBuf,
        output: &mut Vec<PathBuf>,
    ) -> Result<(), io::Error> {
        if path.is_dir() {
            for entry in path.read_dir()? {
                let entry = entry?.path();
                get_all_rust_file_paths_inner(entry, output)?;
            }
        } else if path.is_file() && path.extension().and_then(|s| s.to_str()) == Some("rs") {
            output.push(path);
        }
        Ok(())
    }

    let mut result = Vec::new();
    for dir in SOURCE_DIRS {
        get_all_rust_file_paths_inner(PathBuf::from(dir), &mut result)?;
    }
    Ok(result)
}

fn check_license(path: &Path) -> bool {
    let file = fs::read_to_string(path).unwrap();
    file.contains(LICENSE_TEXT)
}

#[test]
fn license() {
    let files = get_all_rust_file_paths().unwrap();
    for file in files {
        if !check_license(&file) {
            panic!("license text for file {} is incorrect", file.display());
        }
    }
}
