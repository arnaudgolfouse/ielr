@Algorithm(lr, 1)

// Very simple expression language, that uses conflict resolution to determine
// precedence.
// Note that a lot of annotations are needed.

Expr: Expr + Expr
	| Expr - Expr
	| Expr * Expr
	| Expr / Expr
	| - Expr
	| INT

// Associativity
@conflict(reduce(Expr, 0) > shift(+))
@conflict(reduce(Expr, 0) > shift(-))
@conflict(reduce(Expr, 1) > shift(+))
@conflict(reduce(Expr, 1) > shift(-))
@conflict(reduce(Expr, 2) > shift(*))
@conflict(reduce(Expr, 2) > shift(/))
@conflict(reduce(Expr, 3) > shift(*))
@conflict(reduce(Expr, 3) > shift(/))

// Precedence
@conflict(shift(*) > reduce(Expr, 0))
@conflict(shift(/) > reduce(Expr, 0))
@conflict(shift(*) > reduce(Expr, 1))
@conflict(shift(/) > reduce(Expr, 1))
@conflict(reduce(Expr, 2) > shift(+))
@conflict(reduce(Expr, 2) > shift(-))
@conflict(reduce(Expr, 3) > shift(+))
@conflict(reduce(Expr, 3) > shift(-))
@conflict(reduce(Expr, 4) > shift(+))
@conflict(reduce(Expr, 4) > shift(-))
@conflict(reduce(Expr, 4) > shift(/))
@conflict(reduce(Expr, 4) > shift(*))

@example(INT + INT)
@example(INT + INT - INT)
@example(INT * INT + INT)
@example(INT * INT + - INT / INT)